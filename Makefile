#opt1D
CC = g++ -std=c++11
CFLAGS = -g -Wall -Wuninitialized -c -O2
LFLAGS = -Wall -Wuninitialized
OMP = -fopenmp

invOut : invMain.o latticeInv.o readDict.o
	$(CC) $(LFLAGS) $(OMP) invMain.o latticeInv.o readDict.o -o invOut
latticeInv.o : latticeInv.cpp latticeInv.h
	$(CC) $(CFLAGS) $(OMP) latticeInv.cpp
readDict.o : readDict.cpp readDict.h
	$(CC) $(CFLAGS) readDict.cpp
clean:
	rm -f core invOut invMain.o latticeInv.o auxiliaryInv.o readDict.o
