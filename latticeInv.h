// lattice.h
#include <cstdio>
#include <string>
#include "math.h"
#include "omp.h"
#include <cstring>
#include <cstdlib>
#include <fstream>
#include <iostream>
#include "readDict.h"

using namespace std;

#define PI 3.141592653589793238463
#define STRINGSIZE 1000
#define LOGOUT true
#define GIGA 1000000000
#define MEGA 1000000
#define KILO 1000




class lattice{

	private:

	string strOutFileName;
	string strOutFilePath;
	string strIntfEvoOut;
	string strBubbleEvoOut;
	string strDropletEvoOut;

	int iIntfWidth, iLenX, iLenY, iLenAr, iThreadNo, iLambda;
	double dAtwoodNo, dS, dKPrime, dMacrEndT, dRho1, dRho2, dReynoldsNo, dKinVisc2, dKinVisc1, dGravConst, dSrfcTens, dBeta, dKappa, dTau2, dTau1, dRelPertAmpl, dRelTauRhoRatio, wght1, wght2, wght3, force;
	bool bParallel, bPrintRho, bPrintU, bPrintP, bPrintIntfProp, bPrintDropletProp, bPrintBubbleProp, DEBUG;

	double *intfEvo, *dropletEvo, *bubbleEvo;
	double *rho, *u_1, *u_2, *prssr, *psi;
	double *f_0, *f_1, *f_2, *f_3, *f_4, *f_5, *f_6, *f_7, *f_8;
	double *fBar_0, *fBar_1, *fBar_2, *fBar_3, *fBar_4, *fBar_5, *fBar_6, *fBar_7, *fBar_8;
	double *g_0, *g_1, *g_2, *g_3, *g_4, *g_5, *g_6, *g_7, *g_8;
	double *gBar_0, *gBar_1, *gBar_2, *gBar_3, *gBar_4, *gBar_5, *gBar_6, *gBar_7, *gBar_8;

	int *nn_1, *nn_2, *nn_3, *nn_4, *nn_5, *nn_6, *nn_7, *nn_8;				// nearest neighbor in dir i
	int *nnn_1, *nnn_2, *nnn_3, *nnn_4, *nnn_5, *nnn_6, *nnn_7, *nnn_8;		// next nearest neighbor in dir i
	double *gradRho_1, *gradRho_2, *gradRho_11, *gradRho_22, *gradRho_12, *gradRhoSq;

	void setBar();
	void allocateArrays();
	void setBubble(int midX, int midY, int radius, int intfWidth, double velocity );
	void setBoundaryNeighbors();
	void boundaryStream();
	void boundaryStream2();
	void setDistr();
	void exponentialFit(double* func, int len, double& growthEXP, double& constEXP);
	bool readParameters();

	public:

	int iIterStep, iIterStop, iIterInterval, iIterBreak, iObserveX, iObserveY;
	double dPertAmpl;
	bool bGnuPlot, bAbort;
	
	lattice();
	void deletePreviousData();
	void exportData();
	void initialise();
	void iterStep();
	void dump(int x, int y);
	void exportPropagation();
	void printLog( char chInput[], bool bExt );


// old param
	void gnuPlotPrint();
	// double findInterface(double pert, double pertOld, int pos);
	// double findInterface2(double pertOld);
	// double findBubbleAmplitude(double ampl, double amplOld, int intfWidth);
	// double findInterface3(double ampl, double amplOld, int intfWidth);
  	// double intfEvoFit(double* intf, double stop, int pert);
	void exportArray(string filename, double* arrayPointer, int len);
	// int cosPert(int pos, int ampl);
	// void perturbInterface(int amplitude);
	// void perturbQuantity(double* quan, int amplitude);
	// void intfEvoFit(double* intf, char* name );
	void findDropletAmplitude(int iCounter);
	void findBubbleAmplitude(int iCounter);
	void findInterface( int iCounter );
	int abort( int it);
};

