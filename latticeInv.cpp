// lattice.cpp

#include "latticeInv.h"


lattice::lattice(){

  bAbort = false;
  if( !readParameters() ){
    printf("lattice::lattice\t\till defined parameters or wrong path.\n");
    bAbort = true;
  }

  if( !bAbort ){
    int x, y, xp, xm, yp, ym;

    if(bPrintIntfProp){
      strIntfEvoOut = strOutFilePath + strOutFileName + "_IntfEvo.csv";
      ofstream ostrIntfEvo(strIntfEvoOut, ofstream::trunc );
    }
    if(bPrintBubbleProp){
      strBubbleEvoOut = strOutFilePath + strOutFileName + "_BubbleEvo.csv";
      ofstream ostrIntfBubble(strBubbleEvoOut, ofstream::trunc );
    }
    if(bPrintDropletProp){
      strDropletEvoOut = strOutFilePath + strOutFileName + "_DropletEvo.csv";
      ofstream ostrIntfDroplet(strDropletEvoOut, ofstream::trunc );
    }

    wght1 = 4.0/9.0;
    wght2 = 1.0/9.0;
    wght3 = 1.0/36.0;
    dRelTauRhoRatio = (dTau2 - dTau1)/(dRho2 - dRho1); 
    dPertAmpl = double(iLenX) * dRelPertAmpl;
    // dPertAmpl = double(iLambda) * dRelPertAmpl;
    allocateArrays();
    intfEvo[0] = dPertAmpl;
    dropletEvo[0] = dPertAmpl;
    bubbleEvo[0] = dPertAmpl;

    intfEvo[1] = dPertAmpl;
    dropletEvo[1] = dPertAmpl;
    bubbleEvo[1] = dPertAmpl;

    for(int b=0; b<iLenAr; b++){ 
    // initialising nearest neighbors with periodic boundary:
      x = b%iLenX;
      y = b/iLenX;
      xp = (b+1)%iLenX;
      xm = (b-1)%iLenX;  if( b==0 ){ xm = iLenX-1; }
      yp = (b/iLenX + 1) * ( 1 - (b+iLenX)/(iLenX*iLenY));
      ym = ( b/iLenX -1); if( b/iLenX == 0){ ym += iLenY; } 

      nn_1[b] = xp + y*iLenX;
      nn_2[b] = x + yp*iLenX;
      nn_3[b] = xm + y*iLenX;
      nn_4[b] = x + ym*iLenX;
      nn_5[b] = xp + yp*iLenX;
      nn_6[b] = xm + yp*iLenX;
      nn_7[b] = xm + ym*iLenX;
      nn_8[b] = xp + ym*iLenX;
    }
    setBoundaryNeighbors();

    for(int b=0; b<iLenAr; b++){ 
    // next nearest neighbors:
      nnn_1[b] = nn_1[ nn_1[b] ];
      nnn_2[b] = nn_2[ nn_2[b] ];
      nnn_3[b] = nn_3[ nn_3[b] ];
      nnn_4[b] = nn_4[ nn_4[b] ];
      nnn_5[b] = nn_5[ nn_5[b] ];
      nnn_6[b] = nn_6[ nn_6[b] ];
      nnn_7[b] = nn_7[ nn_7[b] ];
      nnn_8[b] = nn_8[ nn_8[b] ];
    }
  }
}

// assingns new nearest neighbors for boundary nodes
void lattice::setBoundaryNeighbors(){
  int element; 
  for(int b=0; b<iLenX; b++){
    element = b+iLenX*(iLenY-1);    // top line element

    nn_2[element] = element; nn_5[element] = element; nn_6[element] = element;
    nn_4[b] = b; nn_7[b] = b; nn_8[b] = b;
  }
}

void lattice::initialise(){
  for ( int b=0; b<iLenAr; b++){
    rho[b] = dRho1;
    prssr[b] = 1.0;
    u_1[b] = 0.0;
    u_2[b] = 0.0;
  }
  setBar();
  setDistr();
}

void lattice::setBar(){
  double smAmpl;
  int j;
  int barWidth = iLenY/2;

  for ( int b=0; b<iLenAr; b++){
    j = b/iLenX;     // giving the row
    smAmpl = 0.5*(dRho2+dRho1) + 0.5*(dRho2-dRho1)*tanh(2.0/double(iIntfWidth) * ( double(j - barWidth ) - dPertAmpl*cos( double(b%iLenX) * 2.0*PI/double(iLenX)) ));    
    
    // for multi mode perturbation:
    // smAmpl = 0.5*(dRho2+dRho1) + 0.5*(dRho2-dRho1)*tanh(2.0/double(iIntfWidth) * ( double(j - barWidth ) 
    //   - iPertAmpl*(    0.25*cos( b%iLenX*2.0*PI/double(iLenX))  + sin( b%iLenX*4.0*PI/double(iLenX)) + cos( b%iLenX*12.0*PI/double(iLenX))    )  ));    

    // straight interface:
    // smAmpl = 0.5*(dRho2+dRho1) + 0.5*(dRho2-dRho1)*tanh(2.0/double(iIntfWidth) * ( double(j - barWidth ) ));    
    rho[b] = smAmpl;
  }
}


void lattice::findDropletAmplitude( int iCounter ){
  if(bPrintDropletProp){
    double ampl = dropletEvo[ iCounter - 1 ];
    double dAmplRes = 0.0001 * ampl;
    ampl = ampl - dAmplRes;
    // printf("ampl = %f, dAmplRes = %f\n",ampl, dAmplRes);
    int counterMax = 1000;
    int bestCount = -1;
    double bestFit = -100;
    double minXsq = 1000000.0;
    double Xsq = 0.0;
    double mean = 0.5*(dRho2 + dRho1);
    double delta = 0.5*(dRho2 - dRho1);
    int y;
    double midY = double(iLenY/2);

    for(int counter = 0; counter<=counterMax; counter++ ){
      for( int j=0; j<iLenY; j++){
        y = iLenX/2 + iLenX*j;
        Xsq += pow( rho[y] - ( mean + delta*tanh( 2.0/double(iIntfWidth)*(j - midY+ampl)   )  ) ,2);
      }
      if(Xsq<minXsq){
        minXsq = Xsq;
        bestFit = ampl;
        bestCount = counter;
      }
      Xsq = 0.0;
      ampl += dAmplRes;
    }
    dropletEvo[ iCounter ] =  bestFit;
    printf("findDropletAmplitude: best amplitude = %f\n",bestFit);
    if(bestCount == counterMax){  printf("\twrong interface: highest fit amplitude reached.\n");  }
  }
}
void lattice::findBubbleAmplitude( int iCounter ){
  if(bPrintBubbleProp){
    double ampl = bubbleEvo[ iCounter - 1 ];
    double dAmplRes = 0.0001 * ampl;
    ampl = ampl - dAmplRes;
    // printf("ampl = %f, dAmplRes = %f\n",ampl, dAmplRes);
    int counterMax = 1000;
    int bestCount = -1;
    double bestFit = -100;
    double minXsq = 1000000.0;
    double Xsq = 0.0;
    double mean = 0.5*(dRho2 + dRho1);
    double delta = 0.5*(dRho2 - dRho1);
    int y;
    double midY = double(iLenY/2);

    for(int counter = 0; counter<=counterMax; counter++ ){
      for( int j=0; j<iLenY; j++){
        y = iLenX*j;
        Xsq += pow( rho[y] - ( mean + delta*tanh( 2.0/double(iIntfWidth)*(j - midY-ampl)   )  ) ,2);
      }
      if(Xsq<minXsq){
        minXsq = Xsq;
        bestFit = ampl;
        bestCount = counter;
      }
      Xsq = 0.0;
      ampl += dAmplRes;
    }
    bubbleEvo[ iCounter ] =  bestFit;
    printf("findBubbleAmplitude: best amplitude = %f\n",bestFit);
    if(bestCount == counterMax){  printf("\twrong interface: highest fit amplitude reached.\n");  }
  }
}

void lattice::findInterface( int iCounter ){
  if(bPrintIntfProp){
    double ampl = intfEvo[ iCounter - 1 ];
    double dAmplRes = 0.0001 * ampl;
    ampl = ampl - dAmplRes;
    // printf("ampl = %f, dAmplRes = %f\n",ampl, dAmplRes);
    int counterMax = 1000;
    int bestCount = -1;
    double bestFit = -100;
    double minXsq = 1000000.0;
    double Xsq = 0.0;
    double mean = 0.5*(dRho2 + dRho1);
    double delta = 0.5*(dRho2 - dRho1);
    double pi = 3.1415926535897;
    double x,y;
    double midY = double(iLenY/2);


    for(int counter = 0; counter<=counterMax; counter++ ){
      for( int b = (int(midY)-int(ampl) + 3*iIntfWidth)*iLenX; b < (int(midY) + int(ampl) + 3*iIntfWidth +1 )*iLenX+1; b++  ){
        x = double(b%iLenX);
        y = double(b/iLenX);
        Xsq += pow(rho[b] - ( mean + delta * tanh(2.0/double(iIntfWidth)* ( y-midY-ampl*cos(x*2.0*pi/double(iLenX) ) ) ) ) ,2);
      }
      if(Xsq<minXsq){
        minXsq = Xsq;
        bestFit = ampl;
        bestCount = counter;
      }
      Xsq = 0.0;
      ampl += dAmplRes;
    }
    intfEvo[ iCounter ] =  bestFit;
    printf("findInterface: best amplitude = %f\n",bestFit);
    if(bestCount == counterMax){  printf("\twrong interface: highest fit amplitude reached.\n");  }
  }
}


void lattice::exportArray(string filename, double* arrayPointer ,int len){
  ofstream outStreamFile(filename, ofstream::app );
  for( int i = 0; i < len; i++){
    outStreamFile << arrayPointer[i] << endl;
  }
}

void lattice::exportPropagation(){
  if(bPrintIntfProp){
    printf("printing in %s\n", strIntfEvoOut.c_str() );
    exportArray(strIntfEvoOut, intfEvo, iIterStop/iIterInterval +  1 );
  }
  if(bPrintBubbleProp){
    printf("printing in %s\n", strBubbleEvoOut.c_str() );
    exportArray(strBubbleEvoOut, bubbleEvo, iIterStop/iIterInterval + 1 );
  }
  if(bPrintDropletProp){
    printf("printing in %s\n", strDropletEvoOut.c_str() );
    exportArray(strDropletEvoOut, dropletEvo, iIterStop/iIterInterval + 1 );
  }
}

void lattice::printLog( char chInput[], bool bExt ){
  char handover[1000];
  const char* filename =  strOutFileName.c_str();
  const char* macr = "logFile_";
  strcpy(handover,macr);
  strcat(handover,filename);
  if( bExt ){
    FILE *logFile = fopen(handover,"a");
    fprintf(logFile, "%s", chInput );
    fclose(logFile);
  } else {
    printf( "%s", chInput );
  }
}

void lattice::deletePreviousData(){
  char handover[1000];
  string strOutFileNamePath = strOutFilePath + strOutFileName;
  const char* filename =  strOutFileNamePath.c_str();

  if( LOGOUT ){
    FILE *LogFile = fopen("logFile", "w" );
    fclose(LogFile);
  }

  if(bPrintRho){
    const char* macr = "_Rho.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    FILE *rhoField = fopen(handover,"w");
    fclose(rhoField); 
  }

  if(bPrintP){
    const char* macr = "_Prssr.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    FILE *prssrField = fopen(handover,"w");
    fclose(prssrField); 
  }

  if(bPrintU){
    const char* macr = "_VelX.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    FILE *velXField = fopen(handover,"w");
    fclose(velXField); 
  }

  if(bPrintU){
    const char* macr = "_VelY.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    FILE *velYField = fopen(handover,"w");
    fclose(velYField); 
  }
}

void lattice::exportData(){
  char handover[1000];
  string strOutFileNamePath = strOutFilePath + strOutFileName;
  const char* filename =  strOutFileNamePath.c_str();

  if(bPrintRho){
    const char* macr = "_Rho.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    printf("lattice::exportData\t\twriting in: %s\n",handover);
    FILE *rhoField = fopen(handover,"a");
    for(int b=0; b<iLenAr-1; b++){
      // fprintf(rhoField,"%.10f,",double(b));    
      fprintf(rhoField,"%.10f,",rho[b]);    
    }
    fprintf(rhoField,"%.10f\n",rho[iLenAr-1]);   
    // fprintf(rhoField,"%.10f\n",double(iLenAr-1));   
    fclose(rhoField); 
  }

  if(bPrintP){
    const char* macr = "_Prssr.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    printf("lattice::exportData\t\twriting in: %s\n",handover);
    FILE *prssrField = fopen(handover,"a");
    for(int b=0; b<iLenAr-1; b++){
      fprintf(prssrField,"%.10f,",prssr[b]);    
    }
    fprintf(prssrField,"%.10f\n",prssr[iLenAr-1]);   
    fclose(prssrField); 
  }

  if(bPrintU){
    const char* macr = "_VelX.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    printf("lattice::exportData\t\twriting in: %s\n",handover);
    FILE *velXField = fopen(handover,"a");
    for(int b=0; b<iLenAr-1; b++){
      fprintf(velXField,"%.10f,",u_1[b]);    
    }
    fprintf(velXField,"%.10f\n",u_1[iLenAr-1]);   
    fclose(velXField); 
  }

  if(bPrintU){
    const char* macr = "_VelY.csv";
    strcpy(handover,filename);
    strcat(handover,macr);
    printf("lattice::exportData\t\twriting in: %s\n",handover);
    FILE *velYField = fopen(handover,"a");
    for(int b=0; b<iLenAr-1; b++){
      fprintf(velYField,"%.10f,",u_2[b]);    
    }
    fprintf(velYField,"%.10f\n",u_2[iLenAr-1]);   
    fclose(velYField); 
  }
}

void lattice::setDistr(){
  double ux, uy, u2, gam, gEqConst;

	for( int b=0; b<iLenAr; b++){
		ux = u_1[b];
		uy = u_2[b];
   	u2 = ux*ux + uy*uy;

   	gam = wght1 * (1.0 - 1.5*u2);          
    gEqConst = 4.0/3.0*prssr[b];
    g_0[b] =  gEqConst + rho[b]*( gam -wght1 );
    f_0[b] = rho[b] * gam;

		gam = wght2 + ( ux +1.5*ux*ux -0.5*u2)/3.0; 
    gEqConst = prssr[b]/3.0;
    g_1[b] =  gEqConst + rho[b]*( gam -wght2 );
    f_1[b] = rho[b] * gam;

    gam = wght2 + ( uy +1.5*uy*uy -0.5*u2)/3.0; 
    g_2[b] =  gEqConst + rho[b]*( gam -wght2 );
    f_2[b] = rho[b] * gam;

    gam = wght2 + ( -ux +1.5*ux*ux -0.5*u2)/3.0; 
    g_3[b] =  gEqConst + rho[b]*( gam -wght2 );
    f_3[b] = rho[b] * gam;

    gam = wght2 + ( -uy +1.5*uy*uy -0.5*u2)/3.0; 
    g_4[b] =  gEqConst + rho[b]*( gam -wght2 );
    f_4[b] = rho[b] * gam;

    gam = wght3 + ( ux+uy +u2 +3.0*ux*uy)/12.0; 
    gEqConst = prssr[b]/12.0;
    g_5[b] =  gEqConst + rho[b]*( gam -wght3 );
    f_5[b] = rho[b] * gam;

    gam = wght3 + ( -ux+uy +u2 -3.0*ux*uy)/12.0; 
    g_6[b] =  gEqConst + rho[b]*( gam -wght3 );
    f_6[b] = rho[b] * gam;

    gam = wght3 + ( -ux-uy +u2 +3.0*ux*uy)/12.0; 
    g_7[b] =  gEqConst + rho[b]*( gam -wght3 );
    f_7[b] = rho[b] * gam;

    gam = wght3 + ( ux-uy +u2 -3.0*ux*uy)/12.0; 
    g_8[b] =  gEqConst + rho[b]*( gam -wght3 );
    f_8[b] = rho[b] * gam;
	} 
}

int lattice::abort( int it){
	// for ( int b=0; b<iLenAr; b++){
    // if (rho[b] != rho[b]){
		if (rho[10] != rho[10]){
      printf("NAN found. iteration stopped at step %d\n",it);
			// printf("NAN found in node %d. iteration stopped at step %d\n",b,it);
			return 0;
		}
	// }
	return -1;
}

// // pos is lattice coordinate
// int lattice::cosPert(int pos, int ampl){
//   int x = pos%iLenX;   
//   double pi = 3.1415926535897;
//   double ret = double(ampl)*cos(pi*2.0/double(iLenX) * double(x));
//   return int(ret);
// }

// void lattice::perturbInterface(int amplitude){
//   perturbQuantity(rho, amplitude);
//   perturbQuantity(prssr, amplitude);
//   perturbQuantity(u_1, amplitude);
//   perturbQuantity(u_2, amplitude);
//   perturbQuantity(psi, amplitude);
//   perturbQuantity(gradRho_1, amplitude);
//   perturbQuantity(gradRho_2, amplitude);
//   perturbQuantity(gradRho_11, amplitude);
//   perturbQuantity(gradRho_22, amplitude);
//   perturbQuantity(gradRho_12, amplitude);

//   perturbQuantity(g_0, amplitude);
//   perturbQuantity(g_1, amplitude);
//   perturbQuantity(g_2, amplitude);
//   perturbQuantity(g_3, amplitude);
//   perturbQuantity(g_4, amplitude);
//   perturbQuantity(g_5, amplitude);
//   perturbQuantity(g_6, amplitude);
//   perturbQuantity(g_7, amplitude);
//   perturbQuantity(g_8, amplitude);

//   perturbQuantity(f_0, amplitude);
//   perturbQuantity(f_1, amplitude);
//   perturbQuantity(f_2, amplitude);
//   perturbQuantity(f_3, amplitude);
//   perturbQuantity(f_4, amplitude);
//   perturbQuantity(f_5, amplitude);
//   perturbQuantity(f_6, amplitude);
//   perturbQuantity(f_7, amplitude);
//   perturbQuantity(f_8, amplitude);

//   perturbQuantity(gBar_0, amplitude);
//   perturbQuantity(gBar_1, amplitude);
//   perturbQuantity(gBar_2, amplitude);
//   perturbQuantity(gBar_3, amplitude);
//   perturbQuantity(gBar_4, amplitude);
//   perturbQuantity(gBar_5, amplitude);
//   perturbQuantity(gBar_6, amplitude);
//   perturbQuantity(gBar_7, amplitude);
//   perturbQuantity(gBar_8, amplitude);

//   perturbQuantity(fBar_0, amplitude);
//   perturbQuantity(fBar_1, amplitude);
//   perturbQuantity(fBar_2, amplitude);
//   perturbQuantity(fBar_3, amplitude);
//   perturbQuantity(fBar_4, amplitude);
//   perturbQuantity(fBar_5, amplitude);
//   perturbQuantity(fBar_6, amplitude);
//   perturbQuantity(fBar_7, amplitude);
//   perturbQuantity(fBar_8, amplitude);  
// }

// void lattice::perturbQuantity(double* quan, int amplitude){
//   double* quanLoc = new double[iLenAr];
//   int shift;
//   for( int b=0; b<iLenAr; b++){
//     quanLoc[b] = quan[b];
//   }
//   for( int b=0; b<iLenAr; b++){
//     shift = b - cosPert(b,amplitude)*iLenX;
//     if( shift < 0){
//       quan[b] = quanLoc[0];
//     } else if ( shift > iLenAr ){
//       quan[b] = quanLoc[iLenAr-1];
//     } else {
//       quan[b] = quanLoc[shift];
//     }
//   }
//   delete [] quanLoc;
// }

// gLoc[0] = gBar_0[b]; gLoc[1] = gBar_1[b]; gLoc[2] = gBar_2[b]; gLoc[3] = gBar_3[b]; gLoc[4] = gBar_4[b]; 
//       gLoc[5] = gBar_5[b]; gLoc[6] = gBar_6[b]; gLoc[7] = gBar_7[b]; gLoc[8] = gBar_8[b];
//       fLoc[0] = fBar_0[b]; fLoc[1] = fBar_1[b]; fLoc[2] = fBar_2[b]; fLoc[3] = fBar_3[b]; fLoc[4] = fBar_4[b]; 
//       fLoc[5] = fBar_5[b]; fLoc[6] = fBar_6[b]; fLoc[7] = fBar_7[b]; fLoc[8] = fBar_8[b];
     
//       drvRho_1 = gradRho_1[b]; drvRho_2 = gradRho_2[b], drvRho_11 = gradRho_11[b]; drvRho_22 = gradRho_22[b], drvRho_12 = gradRho_12[b];

//       rhoLoc[0] = fLoc[0] + fLoc[1] + fLoc[2] + fLoc[3] + fLoc[4] + fLoc[5] + fLoc[6] + fLoc[7] + fLoc[8];
      
//       ux = ( gLoc[1] - gLoc[3] + gLoc[5] - gLoc[7] + gLoc[8] - gLoc[6] + 0.5*dKappa*( drvRho_2*drvRho_12 - drvRho_1*drvRho_22) )/rhoLoc[0];
//       // uy = ( gLoc[2] - gLoc[4] + gLoc[5] - gLoc[7] + gLoc[6] - gLoc[8] + 0.5*dKappa*( drvRho_1*drvRho_12 - drvRho_2*drvRho_11) )/rhoLoc[0];
//       uy = ( gLoc[2] - gLoc[4] + gLoc[5] - gLoc[7] + gLoc[6] - gLoc[8] + 0.5*dKappa*( drvRho_1*drvRho_12 - drvRho_2*drvRho_11) )/rhoLoc[0] - 0.5*dGravConst;
//       prssrLoc = ( gLoc[0] + gLoc[1] + gLoc[2] + gLoc[3] + gLoc[4] + gLoc[5] + gLoc[6] + gLoc[7] + gLoc[8] + 0.5*( ux*drvRho_1 + uy*drvRho_2  ) )/3.0;
//       psiLoc[0] = 4.0*dBeta*(rhoLoc[0] - dRho1)*(rhoLoc[0] - dRho2)*(rhoLoc[0] -0.5*(dRho1 + dRho2)) - dKappa*(drvRho_11 + drvRho_22);

//       rho[b] = rhoLoc[0];
//       u_1[b] = ux;
//       u_2[b] = uy;
//       prssr[b] = prssrLoc;
//       psi[b] = psiLoc[0];
//     }

  // //string strOutoutName;
  // char handover[1000];
  // string strOutFileNamePath = strOutFilePath + strOutFileName;
  // const char* filename =  strOutFileNamePath.c_str();

  // if(bPrintRho){
  //   const char* macr = "_Rho.csv";
  //   strcpy(handover,filename);
  //   strcat(handover,macr);
  //   printf("lattice::exportData\t\twriting in: %s\n",handover);
  //   FILE *rhoField = fopen(handover,"a");
  //   for(int b=0; b<iLenAr-1; b++){
  //     fprintf(rhoField,"%.10f, ",rho[b]);    
  //   }
  //   fprintf(rhoField,"%.10f\n",rho[iLenAr-1]);   
  //   fclose(rhoField); 
  // }


void lattice::gnuPlotPrint(){	
	int x,y;
  double alpha;

  char handover[1000];
  string strOutFileNamePath = strOutFilePath + strOutFileName;
  const char* filename =  strOutFileNamePath.c_str();
  const char* macr = "_GnuPlotOut.csv";
  strcpy(handover,filename);
  strcat(handover,macr);

  FILE *gnuPlotFile;
  gnuPlotFile = fopen(handover,"w");
  
  // for ( int b=0; b<10*iLenX; b++){
  for ( int b=0; b<iLenAr; b++){
  	x = b%iLenX;
  	y = b/iLenX;
  	alpha = rho[b];
    fprintf(gnuPlotFile,"%d \t %d \t %.10f \n",y,x, alpha);
  } 
    
  fclose(gnuPlotFile);

  FILE *gp;
  gp = popen("gnuplot -persist","w");
  fprintf(gp, "set terminal x11 0\n");
  fprintf(gp, "plot \"%s\" using 2:1:3 with image\n",handover);
  // fprintf(gp, "plot \"densityMapFinal.csv\" using 2:1:3 with image\n");
  fclose(gp);

 // -----------------------densityCut----------------------
  FILE *gnuPlotFile2;
  gnuPlotFile2 = fopen("densityCut.csv","w");
  int b;
  // for ( int b=0; b<iLenAr; b++){
  // for ( int b=iLenX/2; b<iLenAr; b = b+iLenX){
  for ( int i=0; i<iLenY; i++){
    b  = iLenX/2 + i*iLenX;
    // j = b;
    alpha = rho[b];
    fprintf(gnuPlotFile2,"%d \t %.10f \n",i,alpha);
  } 
  fclose(gnuPlotFile2);

  FILE *gp2;
  gp2 = popen("gnuplot -persist","w");
  fprintf(gp2, "set terminal x11 1\n");
  fprintf(gp2, "plot \"densityCut.csv\" \n");
  // fprintf(gp2, "plot \"densityCut.csv\", %f+%f*tanh(0.4*(x-%d+%f)) \n",0.5*(dRho2+dRho1),0.5*(dRho2-dRho1),iLenY/2,intf[100]);
  //     Xsq += pow( rho[ iLenX/2 + i*iLenX ] - (mean + delta *tanh( 2.0/5.0*( double(i - iLenY/2+iLenX/10) + c ) ) ) ,2);

  fclose(gp2);
}

void lattice::dump(int x, int y){
	int b = y*iLenX+x;

	printf("\ndump node: x=%d, y=%d\t\tpos in array=%d\n",x,y,b);
	printf("\trho = %.4f\n",rho[b]);
	printf("\tprssr = %.8f\n",prssr[b]);
	printf("\tu_1 = %f, u_2 = %.8f\n",u_1[b],u_2[b]);

	printf("\tnn_i = (%d, %d,  %d, %d, %d, %d, %d, %d )\n",nn_1[b],nn_2[b],nn_3[b],nn_4[b],nn_5[b],nn_6[b],nn_7[b],nn_8[b]);
	printf("\tgBar_i = ( %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f )\n",gBar_0[b],gBar_1[b],gBar_2[b],gBar_3[b],gBar_4[b],gBar_5[b],gBar_6[b],gBar_7[b],gBar_8[b]);
	printf("\tg_i = ( %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f, %.3f )\n",g_0[b],g_1[b],g_2[b],g_3[b],g_4[b],g_5[b],g_6[b],g_7[b],g_8[b]);
	printf("\tfBar_i = ( %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f )\n",fBar_0[b],fBar_1[b],fBar_2[b],fBar_3[b],fBar_4[b],fBar_5[b],fBar_6[b],fBar_7[b],fBar_8[b]);
	printf("\tf_i = ( %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f, %.6f )\n",f_0[b],f_1[b],f_2[b],f_3[b],f_4[b],f_5[b],f_6[b],f_7[b],f_8[b]);
	//printf("\tdirDerivRho_i = (%.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f, %.5f)\n",dirDerivRho_1[b],dirDerivRho_2[b],dirDerivRho_3[b],
	//	dirDerivRho_4[b],dirDerivRho_5[b],dirDerivRho_6[b],dirDerivRho_7[b],dirDerivRho_8[b]);
}

void lattice::allocateArrays(){

  intfEvo = new double[ iIterStop/iIterInterval + 1 ];
  dropletEvo = new double[ iIterStop/iIterInterval + 1 ];
  bubbleEvo = new double[ iIterStop/iIterInterval + 1 ];

	rho = new double[iLenAr];
  u_1 = new double[iLenAr];
  u_2 = new double[iLenAr];
  prssr = new double[iLenAr];
  psi = new double[iLenAr];

	g_0= new double[iLenAr];
	g_1 = new double[iLenAr];
	g_2 = new double[iLenAr];
	g_3 = new double[iLenAr];
	g_4 = new double[iLenAr];
	g_5 = new double[iLenAr];
	g_6 = new double[iLenAr];
	g_7 = new double[iLenAr];
	g_8 = new double[iLenAr];

	gBar_0 = new double[iLenAr];
	gBar_1 = new double[iLenAr];
	gBar_2 = new double[iLenAr];
	gBar_3 = new double[iLenAr];
	gBar_4 = new double[iLenAr];
	gBar_5 = new double[iLenAr];
	gBar_6 = new double[iLenAr];
	gBar_7 = new double[iLenAr];
	gBar_8 = new double[iLenAr];

	f_0 = new double[iLenAr];
	f_1 = new double[iLenAr];
	f_2 = new double[iLenAr];
	f_3 = new double[iLenAr];
	f_4 = new double[iLenAr];
	f_5 = new double[iLenAr];
	f_6 = new double[iLenAr];
	f_7 = new double[iLenAr];
	f_8 = new double[iLenAr];

	fBar_0 = new double[iLenAr];
	fBar_1 = new double[iLenAr];
	fBar_2 = new double[iLenAr];
	fBar_3 = new double[iLenAr];
	fBar_4 = new double[iLenAr];
	fBar_5 = new double[iLenAr];
	fBar_6 = new double[iLenAr];
	fBar_7 = new double[iLenAr];
	fBar_8 = new double[iLenAr];

	// first neighbor in dir e_1, .... second neighbor in dir e_1,....
	nn_1 = new int[iLenAr];
	nn_2 = new int[iLenAr];
	nn_3 = new int[iLenAr];
	nn_4 = new int[iLenAr];
	nn_5 = new int[iLenAr];
	nn_6 = new int[iLenAr];
	nn_7 = new int[iLenAr];
	nn_8 = new int[iLenAr];

	nnn_1 = new int[iLenAr];
	nnn_2 = new int[iLenAr];
	nnn_3 = new int[iLenAr];
	nnn_4 = new int[iLenAr];
	nnn_5 = new int[iLenAr];
	nnn_6 = new int[iLenAr];
	nnn_7 = new int[iLenAr];
	nnn_8 = new int[iLenAr];

  gradRho_1 = new double[iLenAr];
  gradRho_2 = new double[iLenAr];
  gradRho_11 = new double[iLenAr];
  gradRho_22 = new double[iLenAr];
  gradRho_12 = new double[iLenAr];
  gradRhoSq = new double[iLenAr];
}


bool lattice::readParameters(){

  // -----------------------------reading dictinoary---------------------------
  readDict rd("parametersInv.txt");

  strOutFileName = rd.getKeyWordValue<string>("fileName");
  strOutFilePath = rd.getKeyWordValue<string>("fileOutputPath");
  DEBUG =  rd.getKeyWordValue<bool>("debug");
  iLenX = rd.getKeyWordValue<int>("lengthX");
  iIntfWidth = rd.getKeyWordValue<int>("intfWidth");
  dRelPertAmpl = rd.getKeyWordValue<double>("perturbationAmplitude");  //  dRelPertAmpl = stod(strLineIn)/100.0;
  iLambda = rd.getKeyWordValue<int>("perturbationWaveLengthLambda");
  dAtwoodNo = rd.getKeyWordValue<double>("AtwoodNumber");
  dS = rd.getKeyWordValue<double>("surfaceTensionRelatedS");
  dKPrime = rd.getKeyWordValue<double>("kStar");
  dKinVisc2 = rd.getKeyWordValue<double>("kinVisc");
  dMacrEndT = rd.getKeyWordValue<double>("macrTime");
  iIterStop = rd.getKeyWordValue<int>("iterationSteps");
  iIterInterval = rd.getKeyWordValue<int>("outputInterval");
  iThreadNo = rd.getKeyWordValue<int>("numberOfThreads");
  bPrintRho =  rd.getKeyWordValue<bool>("printDensityField");
  bPrintU =  rd.getKeyWordValue<bool>("printVelocityField");
  bPrintP =  rd.getKeyWordValue<bool>("printPressureField");
  bPrintIntfProp =  rd.getKeyWordValue<bool>("InterfaceFit");
  bPrintDropletProp =  rd.getKeyWordValue<bool>("DropletFit");
  bPrintBubbleProp =  rd.getKeyWordValue<bool>("BubbleFit");
  bGnuPlot =  rd.getKeyWordValue<bool>("gnuplotOutput");
  iObserveX = rd.getKeyWordValue<int>("debugCoodinateX");
  iObserveY = rd.getKeyWordValue<int>("debugCoodinateY");


  // ifstream iStrParas("parametersInv.txt");
  // if ( iStrParas.is_open() == 0 ){
  //   printf("lattice::readParameters\t\tno parameterfile found.\n");
  //   return false;
  // }
  // string strLineIn;

  // getline( iStrParas, strOutFileName );
  // getline( iStrParas, strOutFileName );

  // getline( iStrParas, strOutFilePath );
  // getline( iStrParas, strOutFilePath );

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // iLenX = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // iIntfWidth = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // dRelPertAmpl = stod(strLineIn)/100.0;

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // iLambda = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // dAtwoodNo = stod(strLineIn);
  
  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // dS = stod(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // dKPrime = stod(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // dKinVisc2 = stod(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // dMacrEndT = stod(strLineIn);  

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // iIterStop = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // iIterInterval = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // iThreadNo = stoi(strLineIn);    

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // bPrintRho = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // bPrintU = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // bPrintP = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // bPrintIntfProp = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // bPrintDropletProp = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // bPrintBubbleProp = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // bGnuPlot = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // iObserveX = stoi(strLineIn);

  // getline( iStrParas, strLineIn );
  // getline( iStrParas, strLineIn );
  // iObserveY = stoi(strLineIn);

  strOutFileName = strOutFileName + "iLenX" + to_string(iLenX);
  strOutFilePath = getenv("HOME") + strOutFilePath;
  // iLenY = iLenX;
  iLenY = 2*iLenX;
  // iLenY = 400;
  iLenAr = iLenX*iLenY;
  // dRho1 = 1000.0;
  dRho1 = 1.0;
  dRho2 = dRho1 * ( 1.0 + dAtwoodNo ) / ( 1.0 - dAtwoodNo ); 
  //dGravConst = ( 2.0*PI/double(iLambda) ) * ( 2.0*PI/double(iLambda) ) * ( 2.0*PI/double(iLambda) ) * dKinVisc1 * dKinVisc1 / ( dKPrime*dKPrime*dKPrime );
  dGravConst = 2.2*0.000001*double(iLambda)/double(iLenX);
  //dKinVisc2 = pow( pow( ( dKPrime*double(iLambda) )/ (2.0*PI) , 3 ) * dGravConst , 0.5 );
  dKinVisc1 = dKinVisc2;
  dSrfcTens = dS * pow( dGravConst * dKinVisc1*dKinVisc1*dKinVisc1*dKinVisc1 , 1.0/3.0 ) * ( dRho2 + dRho1 );
  dBeta = 12.0*dSrfcTens/ ( pow( dRho2-dRho1 , 4 ) * double( iIntfWidth ) );
  dKappa = 1.5 * double( iIntfWidth ) * dSrfcTens/( pow( dRho2-dRho1 , 2 ) );
  dTau2 = 3.0 * dKinVisc2;
  dTau1 = dTau2;
  iIterStep = 0;
  iIterBreak = iIterInterval;

  if( dMacrEndT > 0.0 ){
    iIterStop = int( dMacrEndT * sqrt( double( iLenX )/dGravConst ) );
    iIterInterval = iIterStop/100;
    iIterStop = 100*iIterInterval;
    iIterBreak = iIterInterval;
    printf("using macroscopic time: iterStop = %d with interval %d \n", iIterStop, iIterInterval);
  }

  if(iThreadNo>1){
    bParallel = true;
  } else {
    bParallel = false;
  }

  printf("initial parameters:\n\
      grid:\t\tiLenX = %d, iLenY = %d\n\
      RT paras:\t\tk* = %f, S = %f\n\
      interface width = %d\n\
      perturbation wave length = %d\n\
      density\t\trho2 = %.1f, rho1 = %.1f\n\
      debug\t\tobserveX = %d, observeY = %d\n\
      Reynolds number = %.1f\n\
      kin Viscosity 2,1: %.5f,%.5f\n\
      gravConst = %.5f 10⁻9\n\
      surface tension = %.5f 10⁻6\n\
      dBeta = %.5f 10⁻6\tdKappa = %.5f 10⁻6\n\
      tau2 = %f, tau1 = %f\n\
      iteration stop = %d, data export intervall = %d\n\
      no of threads = %d\n\
      output:\tRho : %d, U : %d, P : %d, Intf : %d, Gnuplot : %d\n"
      ,iLenX,iLenY,dKPrime,dS, iIntfWidth, iLambda, dRho2,dRho1,iObserveX,iObserveY,
       dReynoldsNo, dKinVisc2, dKinVisc1, GIGA*dGravConst, MEGA*dSrfcTens, MEGA*dBeta, MEGA*dKappa, dTau2, dTau1, iIterStop, iIterInterval, iThreadNo, bPrintRho, bPrintU, bPrintP, bPrintIntfProp, bGnuPlot);

  string strOutFileNamePath = strOutFilePath + strOutFileName + "_parameters.txt" ;
  const char * pChOutFileNamePath = strOutFileNamePath.c_str();
  FILE *parameterPrint = fopen(pChOutFileNamePath,"w");
  if ( parameterPrint == NULL ){
    cout << "lattice::readParameters\toutput path does not exist:\t" << pChOutFileNamePath << endl;
    // printf("lattice::readParameters\toutput path does not exist.\n");
    return false;
  }
  fprintf(parameterPrint,"initial parameters:\n\
      grid:\t\tiLenX = %d, iLenY = %d\n\
      RT paras:\t\tk* = %f, S = %f\n\
      interface width = %d\n\
      perturbation wave length = %d\n\
      density\t\trho2 = %.1f, rho1 = %.1f\n\
      debug\t\tobserveX = %d, observeY = %d\n\
      Reynolds number = %.1f\n\
      kin Viscosity 2,1: %.5f,%.5f\n\
      gravConst = %.5f 10⁻9\n\
      surface tension = %.5f 10⁻6\n\
      dBeta = %.5f 10⁻6\tdKappa = %.5f 10⁻6\n\
      tau2 = %f, tau1 = %f\n\
      iteration stop = %d, data export intervall = %d\n\
      no of threads = %d\n\
      output:\tRho : %d, U : %d, P : %d, Intf : %d, Gnuplot : %d\n"
      ,iLenX,iLenY,dKPrime,dS, iIntfWidth, iLambda, dRho2,dRho1,iObserveX,iObserveY,
       dReynoldsNo, dKinVisc2, dKinVisc1, GIGA*dGravConst, MEGA*dSrfcTens, MEGA*dBeta, MEGA*dKappa, dTau2, dTau1, iIterStop, iIterInterval, iThreadNo, bPrintRho, bPrintU, bPrintP, bPrintIntfProp, bGnuPlot);
  fclose(parameterPrint);

  // with these parameters, the simulation is expected stable.
  return true;
}

void lattice::iterStep(){

  omp_set_num_threads(iThreadNo);
  #pragma omp parallel default(none) if( bParallel )
  {
  // --------------------------------------------------allocate local variables-------------------------------------------------
    int e1, e2, e3, e4, e5, e6, e7, e8, element;
    double prssrLoc, gam, gEqConst, gEq, frc, tau, ux, uy, u2, degRate, degRate2, GX, GY, GU, inv12 = 1.0/12.0;
    double drvRho_1, drvRho_2, drvRho_11, drvRho_22, drvRho_12, gradRhoU, gradPsiU;

    double dUY, velConstB,velConstA;

    double gLoc[9]; 
    double fLoc[9]; 
    double rhoLoc[17]; 
    double psiLoc[17]; 
    double dirDerivRhoLoc[9];
    double dirDerivPsiLoc[9];
  
  // ----------------------------------------------------set first derivatives-------------------------------------------------
    #pragma omp for
    for( int b=0; b<iLenAr; b++){
    // for( int b=iLenX; b<iLenAr; b++){
    //   local field of Nearest Neighbors (NN)
      e1 = nn_1[b];  e2 = nn_2[b]; e3 = nn_3[b]; e4 = nn_4[b]; e5 = nn_5[b]; e6 = nn_6[b]; e7 = nn_7[b]; e8 = nn_8[b];

      rhoLoc[1] = rho[e1]; rhoLoc[2] = rho[e2]; rhoLoc[3] = rho[e3]; rhoLoc[4] = rho[e4];
      rhoLoc[5] = rho[e5]; rhoLoc[6] = rho[e6]; rhoLoc[7] = rho[e7]; rhoLoc[8] = rho[e8];

      drvRho_1 = (4.0*rhoLoc[1] - 4.0*rhoLoc[3] + rhoLoc[5] - rhoLoc[7] + rhoLoc[8] - rhoLoc[6] )*inv12;
      drvRho_2 = (4.0*rhoLoc[2] - 4.0*rhoLoc[4] + rhoLoc[5] - rhoLoc[7] + rhoLoc[6] - rhoLoc[8] )*inv12;

      gradRhoSq[b] = drvRho_1*drvRho_1 + drvRho_2*drvRho_2; 
      gradRho_1[b] = drvRho_1;
      gradRho_2[b] = drvRho_2; 

      gradRho_11[b] = drvRho_1*drvRho_1;
      gradRho_22[b] = drvRho_2*drvRho_2;
      gradRho_12[b] = drvRho_1*drvRho_2;
    }

    // ----------------------------------------------------set second derivatives-------------------------------------------------
    // #pragma omp for
    // for( int b=0; b<iLenAr; b++){
    // // for( int b=0; b<iLenAr-iLenX; b++){
    //   e1 = nn_1[b];  e2 = nn_2[b]; e3 = nn_3[b]; e4 = nn_4[b]; e5 = nn_5[b]; e6 = nn_6[b]; e7 = nn_7[b]; e8 = nn_8[b];

    //   rhoLoc[1] = gradRho_1[e1]; rhoLoc[2] = gradRho_1[e2]; rhoLoc[3] = gradRho_1[e3]; rhoLoc[4] = gradRho_1[e4];
    //   rhoLoc[5] = gradRho_1[e5]; rhoLoc[6] = gradRho_1[e6]; rhoLoc[7] = gradRho_1[e7]; rhoLoc[8] = gradRho_1[e8];

    //   psiLoc[1] = gradRho_2[e1]; psiLoc[2] = gradRho_2[e2]; psiLoc[3] = gradRho_2[e3]; psiLoc[4] = gradRho_2[e4];
    //   psiLoc[5] = gradRho_2[e5]; psiLoc[6] = gradRho_2[e6]; psiLoc[7] = gradRho_2[e7]; psiLoc[8] = gradRho_2[e8];

    //   drvRho_11 = (4.0*rhoLoc[1] - 4.0*rhoLoc[3] + rhoLoc[5] - rhoLoc[7] + rhoLoc[8] - rhoLoc[6] )*inv12;
    //   drvRho_12 = (4.0*rhoLoc[2] - 4.0*rhoLoc[4] + rhoLoc[5] - rhoLoc[7] + rhoLoc[6] - rhoLoc[8] )*inv12;      
    //   drvRho_22 = (4.0*psiLoc[2] - 4.0*psiLoc[4] + psiLoc[5] - psiLoc[7] + psiLoc[6] - psiLoc[8] )*inv12;

    //   gradRho_11[b] = drvRho_11; 
    //   gradRho_22[b] = drvRho_22;
    //   gradRho_12[b] = drvRho_12;
    // }
  // ----------------------------------------------------preStrCollAndStr-------------------------------------------------
    #pragma omp for nowait
    for( int b=iLenX; b<iLenAr-iLenX; b++){
    // for( int b=0; b<iLenAr; b++){

      gLoc[0] = g_0[b]; gLoc[1] = g_1[b]; gLoc[2] = g_2[b]; gLoc[3] = g_3[b]; gLoc[4] = g_4[b]; 
      gLoc[5] = g_5[b]; gLoc[6] = g_6[b]; gLoc[7] = g_7[b]; gLoc[8] = g_8[b];
      fLoc[0] = f_0[b]; fLoc[1] = f_1[b]; fLoc[2] = f_2[b]; fLoc[3] = f_3[b]; fLoc[4] = f_4[b]; 
      fLoc[5] = f_5[b]; fLoc[6] = f_6[b]; fLoc[7] = f_7[b]; fLoc[8] = f_8[b];

      // local field of Next Nearest Neighbors (NNN)
      e1 = nnn_1[b];  e2 = nnn_2[b]; e3 = nnn_3[b]; e4 = nnn_4[b]; e5 = nnn_5[b]; e6 = nnn_6[b]; e7 = nnn_7[b]; e8 = nnn_8[b];
      rhoLoc[9] = rho[e1]; rhoLoc[10] = rho[e2]; rhoLoc[11] = rho[e3]; rhoLoc[12] = rho[e4];
      rhoLoc[13] = rho[e5]; rhoLoc[14] = rho[e6]; rhoLoc[15] = rho[e7]; rhoLoc[16] = rho[e8];

      psiLoc[9] = psi[e1]; psiLoc[10] = psi[e2]; psiLoc[11] = psi[e3]; psiLoc[12] = psi[e4];
      psiLoc[13] = psi[e5]; psiLoc[14] = psi[e6]; psiLoc[15] = psi[e7]; psiLoc[16] = psi[e8];

      // local field of Nearest Neighbors (NN)
      e1 = nn_1[b];  e2 = nn_2[b]; e3 = nn_3[b]; e4 = nn_4[b]; e5 = nn_5[b]; e6 = nn_6[b]; e7 = nn_7[b]; e8 = nn_8[b];

      rhoLoc[0] = rho[b]; rhoLoc[1] = rho[e1]; rhoLoc[2] = rho[e2]; rhoLoc[3] = rho[e3]; rhoLoc[4] = rho[e4];
      rhoLoc[5] = rho[e5]; rhoLoc[6] = rho[e6]; rhoLoc[7] = rho[e7]; rhoLoc[8] = rho[e8];

      psiLoc[0] = psi[b]; psiLoc[1] = psi[e1]; psiLoc[2] = psi[e2]; psiLoc[3] = psi[e3]; psiLoc[4] = psi[e4];
      psiLoc[5] = psi[e5]; psiLoc[6] = psi[e6]; psiLoc[7] = psi[e7]; psiLoc[8] = psi[e8];


      // -------------------------calculating derivatives------------------------------

      dirDerivRhoLoc[1] = 0.5*( -rhoLoc[9] +4.0*rhoLoc[1] -3.0*rhoLoc[0] );
      if( (rhoLoc[9] - rhoLoc[0])*dirDerivRhoLoc[1] < 0.0 ) dirDerivRhoLoc[1] = 0.5*( rhoLoc[1] - rhoLoc[3] );

      dirDerivRhoLoc[2] = 0.5*( -rhoLoc[10] +4.0*rhoLoc[2] -3.0*rhoLoc[0] );
      if( (rhoLoc[10] - rhoLoc[0])*dirDerivRhoLoc[2] < 0.0 ) dirDerivRhoLoc[2] = 0.5*( rhoLoc[2] - rhoLoc[4] );

      dirDerivRhoLoc[3] = 0.5*( -rhoLoc[11] +4.0*rhoLoc[3] -3.0*rhoLoc[0] );
      if( (rhoLoc[11] - rhoLoc[0])*dirDerivRhoLoc[3] < 0.0 ) dirDerivRhoLoc[3] = 0.5*( rhoLoc[3] - rhoLoc[1] );

      dirDerivRhoLoc[4] = 0.5*( -rhoLoc[12] +4.0*rhoLoc[4] -3.0*rhoLoc[0] );
      if( (rhoLoc[12] - rhoLoc[0])*dirDerivRhoLoc[4] < 0.0 ) dirDerivRhoLoc[4] = 0.5*( rhoLoc[4] - rhoLoc[2] );

      dirDerivRhoLoc[5] = 0.5*( -rhoLoc[13] +4.0*rhoLoc[5] -3.0*rhoLoc[0] );
      if( (rhoLoc[13] - rhoLoc[0])*dirDerivRhoLoc[5] < 0.0 ) dirDerivRhoLoc[5] = 0.5*( rhoLoc[5] - rhoLoc[7] );

      dirDerivRhoLoc[6] = 0.5*( -rhoLoc[14] +4.0*rhoLoc[6] -3.0*rhoLoc[0] );
      if( (rhoLoc[14] - rhoLoc[0])*dirDerivRhoLoc[6] < 0.0 ) dirDerivRhoLoc[6] = 0.5*( rhoLoc[6] - rhoLoc[8] );

      dirDerivRhoLoc[7] = 0.5*( -rhoLoc[15] +4.0*rhoLoc[7] -3.0*rhoLoc[0] );
      if( (rhoLoc[15] - rhoLoc[0])*dirDerivRhoLoc[7] < 0.0 ) dirDerivRhoLoc[7] = 0.5*( rhoLoc[7] - rhoLoc[5] );

      dirDerivRhoLoc[8] = 0.5*( -rhoLoc[16] +4.0*rhoLoc[8] -3.0*rhoLoc[0] );
      if( (rhoLoc[16] - rhoLoc[0])*dirDerivRhoLoc[8] < 0.0 ) dirDerivRhoLoc[8] = 0.5*( rhoLoc[8] - rhoLoc[6] );

      dirDerivPsiLoc[1] = 0.5*( -psiLoc[9] +4.0*psiLoc[1] -3.0*psiLoc[0] );
      if( (psiLoc[9] - psiLoc[0])*dirDerivPsiLoc[1] < 0.0 ) dirDerivPsiLoc[1] = 0.5*( psiLoc[1] - psiLoc[3] );

      dirDerivPsiLoc[2] = 0.5*( -psiLoc[10] +4.0*psiLoc[2] -3.0*psiLoc[0] );
      if( (psiLoc[10] - psiLoc[0])*dirDerivPsiLoc[2] < 0.0 ) dirDerivPsiLoc[2] = 0.5*( psiLoc[2] - psiLoc[4] );

      dirDerivPsiLoc[3] = 0.5*( -psiLoc[11] +4.0*psiLoc[3] -3.0*psiLoc[0] );
      if( (psiLoc[11] - psiLoc[0])*dirDerivPsiLoc[3] < 0.0 ) dirDerivPsiLoc[3] = 0.5*( psiLoc[3] - psiLoc[1] );

      dirDerivPsiLoc[4] = 0.5*( -psiLoc[12] +4.0*psiLoc[4] -3.0*psiLoc[0] );
      if( (psiLoc[12] - psiLoc[0])*dirDerivPsiLoc[4] < 0.0 ) dirDerivPsiLoc[4] = 0.5*( psiLoc[4] - psiLoc[2] );

      dirDerivPsiLoc[5] = 0.5*( -psiLoc[13] +4.0*psiLoc[5] -3.0*psiLoc[0] );
      if( (psiLoc[13] - psiLoc[0])*dirDerivPsiLoc[5] < 0.0 ) dirDerivPsiLoc[5] = 0.5*( psiLoc[5] - psiLoc[7] );

      dirDerivPsiLoc[6] = 0.5*( -psiLoc[14] +4.0*psiLoc[6] -3.0*psiLoc[0] );
      if( (psiLoc[14] - psiLoc[0])*dirDerivPsiLoc[6] < 0.0 ) dirDerivPsiLoc[6] = 0.5*( psiLoc[6] - psiLoc[8] );

      dirDerivPsiLoc[7] = 0.5*( -psiLoc[15] +4.0*psiLoc[7] -3.0*psiLoc[0] );
      if( (psiLoc[15] - psiLoc[0])*dirDerivPsiLoc[7] < 0.0 ) dirDerivPsiLoc[7] = 0.5*( psiLoc[7] - psiLoc[5] );

      dirDerivPsiLoc[8] = 0.5*( -psiLoc[16] +4.0*psiLoc[8] -3.0*psiLoc[0] );
      if( (psiLoc[16] - psiLoc[0])*dirDerivPsiLoc[8] < 0.0 ) dirDerivPsiLoc[8] = 0.5*( psiLoc[8] - psiLoc[6] );

      prssrLoc = prssr[b];

      ux = u_1[b]; 
      uy = u_2[b]; 
      u2 = ux*ux + uy*uy;

      dUY = -dGravConst;
      velConstA = 4.0*dUY*( uy + dUY);
      velConstB = dUY*(dUY - 2.0*uy);

      tau = dTau1 + (rhoLoc[0] - dRho1)*dRelTauRhoRatio;
      degRate = 0.5/tau;
      drvRho_1 = gradRho_1[b]; drvRho_2 = gradRho_2[b];
      gradRhoU = ux*drvRho_1 + uy*drvRho_2;

      // arraysentries abspeichern?
      GX = 1.5*dKappa*( ( 4.0*gradRhoSq[e1] - 4.0*gradRhoSq[e3] + gradRhoSq[e5] - gradRhoSq[e7] + gradRhoSq[e8] - gradRhoSq[e6] )*inv12 -      // del_x(del_j Rho)^2
        ( 4.0*gradRho_11[e1] - 4.0*gradRho_11[e3] + gradRho_11[e5] - gradRho_11[e7] + gradRho_11[e8] - gradRho_11[e6] )*inv12 -   // del_x(del_xx rho)
        ( 4.0*gradRho_12[e2] - 4.0*gradRho_12[e4] + gradRho_12[e5] - gradRho_12[e7] + gradRho_12[e6] - gradRho_12[e8] )*inv12 );    // del_y(del_xy rho)
      GY = 1.5*dKappa*( ( 4.0*gradRhoSq[e2] - 4.0*gradRhoSq[e4] + gradRhoSq[e5] - gradRhoSq[e7] + gradRhoSq[e6] - gradRhoSq[e8] )*inv12 -
        ( 4.0*gradRho_22[e2] - 4.0*gradRho_22[e4] + gradRho_22[e5] - gradRho_22[e7] + gradRho_22[e6] - gradRho_22[e8] )*inv12 -
        ( 4.0*gradRho_12[e1] - 4.0*gradRho_12[e3] + gradRho_12[e5] - gradRho_12[e7] + gradRho_12[e8] - gradRho_12[e6] )*inv12 );  
      GU = -ux*GX -uy*GY;

      gradPsiU = ux*( 4.0*psiLoc[1] - 4.0*psiLoc[3] + psiLoc[5] - psiLoc[7] + psiLoc[8] - psiLoc[6] )*inv12 + 
        uy * ( 4.0*psiLoc[2] - 4.0*psiLoc[4] + psiLoc[5] - psiLoc[7] + psiLoc[6] - psiLoc[8] )*inv12;

      //-------------------------e0---------------------------------
      gam = wght1 * (1.0 - 1.5*u2);    
      gEqConst = 4.0/3.0*prssrLoc;
      gEq =  gEqConst + rhoLoc[0]*( gam -wght1 );
      force = 2.0/3.0*rhoLoc[0]*velConstB;

      frc = -0.5*gradRhoU*(gam - wght1) + GU * gam;
      gLoc[0] = gLoc[0] + degRate *( gEq - gLoc[0] ) + frc + 0.5*force; 

      frc = (-0.5*gradRhoU +1.5*rhoLoc[0]*gradPsiU )*gam;
      fLoc[0] = fLoc[0] + degRate*( rhoLoc[0]*gam - fLoc[0] ) + frc;

      //-------------------------e1---------------------------------
      gam = wght2 + ( ux +1.5*ux*ux -0.5*u2)/3.0; 
      gEqConst = prssrLoc/3.0;
      gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*velConstB;

      frc = 0.5* (dirDerivRhoLoc[1] - gradRhoU) * (gam - wght2) + ( GX + GU )*gam;
      gLoc[1] = gLoc[1] + degRate*( gEq - gLoc[1] ) + frc +  0.5*force;  

      frc = 0.5*(dirDerivRhoLoc[1] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[1] - gradPsiU )*gam;
      fLoc[1] = fLoc[1] + degRate*( rhoLoc[0]*gam - fLoc[1] ) + frc;

      //-------------------------e2---------------------------------
      gam = wght2 + ( uy +1.5*uy*uy -0.5*u2)/3.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*(2.0*dUY + velConstA);

      frc = 0.5* (dirDerivRhoLoc[2] - gradRhoU ) * (gam - wght2) + (GY + GU )*gam ;
      gLoc[2] = gLoc[2] + degRate*( gEq - gLoc[2] ) + frc +  0.5*force;  

      frc = 0.5*(dirDerivRhoLoc[2] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[2] - gradPsiU )*gam;
      fLoc[2] = fLoc[2] + degRate*( rhoLoc[0]*gam - fLoc[2] ) + frc;        

      //-------------------------e3---------------------------------
      gam = wght2 + ( -ux +1.5*ux*ux -0.5*u2)/3.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*velConstB;

      frc = 0.5* (dirDerivRhoLoc[3] - gradRhoU) * (gam - wght2) + ( -GX + GU )*gam;
      gLoc[3] = gLoc[3] + degRate*( gEq - gLoc[3] ) + frc +  0.5*force;    

      frc = 0.5*(dirDerivRhoLoc[3] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[3] - gradPsiU )*gam;
      fLoc[3] = fLoc[3] + degRate*( rhoLoc[0]*gam - fLoc[3] ) + frc;

      //-------------------------e4---------------------------------
      gam = wght2 + ( -uy +1.5*uy*uy -0.5*u2)/3.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*(-2.0*dUY + velConstA);

      frc = 0.5* (dirDerivRhoLoc[4] - gradRhoU) * (gam - wght2) + ( -GY + GU )*gam ;
      gLoc[4] = gLoc[4] + degRate*( gEq - gLoc[4] ) + frc +  0.5*force;    

      frc = 0.5*(dirDerivRhoLoc[4] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[4] - gradPsiU )*gam;
      fLoc[4] = fLoc[4] + degRate*( rhoLoc[0]*gam - fLoc[4] ) + frc;

      //-------------------------e5---------------------------------
      gam = wght3 + ( ux+uy +u2 +3.0*ux*uy)/12.0; 
      gEqConst = prssr[b]/12.0;
      gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );
      force = 1.0/24.0*rhoLoc[0]*(2.0*dUY + velConstA);

      frc = 0.5* (dirDerivRhoLoc[5] - gradRhoU) * (gam - wght3) + (GX + GY + GU )*gam;
      gLoc[5] = gLoc[5] + degRate*( gEq - gLoc[5] ) + frc +  0.5*force;

      frc = 0.5*(dirDerivRhoLoc[5] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[5] - gradPsiU )*gam;
      fLoc[5] = fLoc[5] + degRate*( rhoLoc[0]*gam - fLoc[5] ) + frc;    
      
      //-------------------------e6---------------------------------
      gam = wght3 + ( -ux+uy +u2 -3.0*ux*uy)/12.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );   
      force = 1.0/24.0*rhoLoc[0]*(2.0*dUY + velConstA);

      frc = 0.5* (dirDerivRhoLoc[6] - gradRhoU) * (gam - wght3) + ( -GX + GY + GU )*gam;
      gLoc[6] = gLoc[6] + degRate*( gEq - gLoc[6] ) + frc +  0.5*force;

      frc = 0.5*(dirDerivRhoLoc[6] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[6] - gradPsiU )*gam;
      fLoc[6] = fLoc[6] + degRate*( rhoLoc[0]*gam - fLoc[6] ) + frc;    

      //-------------------------e7---------------------------------
      gam = wght3 + ( -ux-uy +u2 +3.0*ux*uy)/12.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );
      force = 1.0/24.0*rhoLoc[0]*(-2.0*dUY + velConstA);

      frc = 0.5* (dirDerivRhoLoc[7] - gradRhoU) * (gam - wght3) + ( -GX -GY + GU )*gam;
      gLoc[7] = gLoc[7] + degRate*( gEq - gLoc[7] ) + frc +  0.5*force;

      frc = 0.5*(dirDerivRhoLoc[7] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[7] - gradPsiU )*gam;
      fLoc[7] = fLoc[7] + degRate*( rhoLoc[0]*gam - fLoc[7] ) + frc;    

      //-------------------------e8---------------------------------
      gam = wght3 + ( ux-uy +u2 -3.0*ux*uy)/12.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );
      force = 1.0/24.0*rhoLoc[0]*(-2.0*dUY + velConstA);

      frc = 0.5* (dirDerivRhoLoc[8] - gradRhoU) * (gam - wght3) + ( GX - GY + GU )*gam;
      gLoc[8] = gLoc[8] + degRate*( gEq - gLoc[8] ) + frc +  0.5*force;  

      frc = 0.5*(dirDerivRhoLoc[8] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[8] - gradPsiU )*gam;
      fLoc[8] = fLoc[8] + degRate*( rhoLoc[0]*gam - fLoc[8] ) + frc;  

      // --------------------------streaming--------------------------

      gBar_0[b] = gLoc[0]; 
      gBar_1[e1] = gLoc[1]; 
      gBar_2[e2] = gLoc[2]; 
      gBar_3[e3] = gLoc[3]; 
      gBar_4[e4] = gLoc[4]; 
      gBar_5[e5] = gLoc[5]; 
      gBar_6[e6] = gLoc[6]; 
      gBar_7[e7] = gLoc[7]; 
      gBar_8[e8] = gLoc[8];

      fBar_0[b] = fLoc[0]; 
      fBar_1[e1] = fLoc[1]; 
      fBar_2[e2] = fLoc[2]; 
      fBar_3[e3] = fLoc[3]; 
      fBar_4[e4] = fLoc[4]; 
      fBar_5[e5] = fLoc[5]; 
      fBar_6[e6] = fLoc[6]; 
      fBar_7[e7] = fLoc[7]; 
      fBar_8[e8] = fLoc[8];
    }

  // ----------------------------------------------------boundary collision-------------------------------------------------
  #pragma omp for nowait
  for( int b=0; b<iLenX; b++){
  // top/bottom boundary: free slip
  // no collision/streaming in 2,5,6 direction

    element = b+iLenX*(iLenY-1);    // top line element

    gLoc[0] = g_0[element]; gLoc[1] = g_1[element]; gLoc[2] = g_2[element]; gLoc[3] = g_3[element]; gLoc[4] = g_4[element]; 
    gLoc[5] = g_5[element]; gLoc[6] = g_6[element]; gLoc[7] = g_7[element]; gLoc[8] = g_8[element];
    fLoc[0] = f_0[element]; fLoc[1] = f_1[element]; fLoc[2] = f_2[element]; fLoc[3] = f_3[element]; fLoc[4] = f_4[element]; 
    fLoc[5] = f_5[element]; fLoc[6] = f_6[element]; fLoc[7] = f_7[element]; fLoc[8] = f_8[element];

    // local field of Next Nearest Neighbors (NNN)
    e1 = nnn_1[element];  e2 = nnn_2[element]; e3 = nnn_3[element]; 
    e4 = nnn_4[element]; e5 = nnn_5[element]; e6 = nnn_6[element]; e7 = nnn_7[element]; e8 = nnn_8[element];
    rhoLoc[9] = rho[e1]; rhoLoc[10] = rho[e2]; rhoLoc[11] = rho[e3]; rhoLoc[12] = rho[e4];
    rhoLoc[13] = rho[e5]; rhoLoc[14] = rho[e6]; rhoLoc[15] = rho[e7]; rhoLoc[16] = rho[e8];

    psiLoc[9] = psi[e1]; psiLoc[10] = psi[e2]; psiLoc[11] = psi[e3]; psiLoc[12] = psi[e4];
    psiLoc[13] = psi[e5]; psiLoc[14] = psi[e6]; psiLoc[15] = psi[e7]; psiLoc[16] = psi[e8];

    // local field of Nearest Neighbors (NN)
    e1 = nn_1[element];  e2 = nn_2[element]; e3 = nn_3[element]; e4 = nn_4[element]; 
    e5 = nn_5[element]; e6 = nn_6[element]; e7 = nn_7[element]; e8 = nn_8[element];

    rhoLoc[0] = rho[element]; rhoLoc[1] = rho[e1]; rhoLoc[2] = rho[e2]; rhoLoc[3] = rho[e3]; rhoLoc[4] = rho[e4];
    rhoLoc[5] = rho[e5]; rhoLoc[6] = rho[e6]; rhoLoc[7] = rho[e7]; rhoLoc[8] = rho[e8];

    psiLoc[0] = psi[element]; psiLoc[1] = psi[e1]; psiLoc[2] = psi[e2]; psiLoc[3] = psi[e3]; psiLoc[4] = psi[e4];
    psiLoc[5] = psi[e5]; psiLoc[6] = psi[e6]; psiLoc[7] = psi[e7]; psiLoc[8] = psi[e8];

    dirDerivRhoLoc[1] = 0.5*( -rhoLoc[9] +4.0*rhoLoc[1] -3.0*rhoLoc[0] );
    if( (rhoLoc[9] - rhoLoc[0])*dirDerivRhoLoc[1] < 0.0 ) dirDerivRhoLoc[1] = 0.5*( rhoLoc[1] - rhoLoc[3] );

    dirDerivRhoLoc[3] = 0.5*( -rhoLoc[11] +4.0*rhoLoc[3] -3.0*rhoLoc[0] );
    if( (rhoLoc[11] - rhoLoc[0])*dirDerivRhoLoc[3] < 0.0 ) dirDerivRhoLoc[3] = 0.5*( rhoLoc[3] - rhoLoc[1] );

    dirDerivRhoLoc[4] = 0.5*( -rhoLoc[12] +4.0*rhoLoc[4] -3.0*rhoLoc[0] );
    if( (rhoLoc[12] - rhoLoc[0])*dirDerivRhoLoc[4] < 0.0 ) dirDerivRhoLoc[4] = 0.5*( rhoLoc[4] - rhoLoc[2] );

    dirDerivRhoLoc[7] = 0.5*( -rhoLoc[15] +4.0*rhoLoc[7] -3.0*rhoLoc[0] );
    if( (rhoLoc[15] - rhoLoc[0])*dirDerivRhoLoc[7] < 0.0 ) dirDerivRhoLoc[7] = 0.5*( rhoLoc[7] - rhoLoc[5] );

    dirDerivRhoLoc[8] = 0.5*( -rhoLoc[16] +4.0*rhoLoc[8] -3.0*rhoLoc[0] );
    if( (rhoLoc[16] - rhoLoc[0])*dirDerivRhoLoc[8] < 0.0 ) dirDerivRhoLoc[8] = 0.5*( rhoLoc[8] - rhoLoc[6] );

    dirDerivPsiLoc[1] = 0.5*( -psiLoc[9] +4.0*psiLoc[1] -3.0*psiLoc[0] );
    if( (psiLoc[9] - psiLoc[0])*dirDerivPsiLoc[1] < 0.0 ) dirDerivPsiLoc[1] = 0.5*( psiLoc[1] - psiLoc[3] );

    dirDerivPsiLoc[3] = 0.5*( -psiLoc[11] +4.0*psiLoc[3] -3.0*psiLoc[0] );
    if( (psiLoc[11] - psiLoc[0])*dirDerivPsiLoc[3] < 0.0 ) dirDerivPsiLoc[3] = 0.5*( psiLoc[3] - psiLoc[1] );

    dirDerivPsiLoc[4] = 0.5*( -psiLoc[12] +4.0*psiLoc[4] -3.0*psiLoc[0] );
    if( (psiLoc[12] - psiLoc[0])*dirDerivPsiLoc[4] < 0.0 ) dirDerivPsiLoc[4] = 0.5*( psiLoc[4] - psiLoc[2] );

    dirDerivPsiLoc[7] = 0.5*( -psiLoc[15] +4.0*psiLoc[7] -3.0*psiLoc[0] );
    if( (psiLoc[15] - psiLoc[0])*dirDerivPsiLoc[7] < 0.0 ) dirDerivPsiLoc[7] = 0.5*( psiLoc[7] - psiLoc[5] );

    dirDerivPsiLoc[8] = 0.5*( -psiLoc[16] +4.0*psiLoc[8] -3.0*psiLoc[0] );
    if( (psiLoc[16] - psiLoc[0])*dirDerivPsiLoc[8] < 0.0 ) dirDerivPsiLoc[8] = 0.5*( psiLoc[8] - psiLoc[6] );

    
    prssrLoc = prssr[element];
    ux = u_1[element]; 
    uy = u_2[element]; 

    u2 = ux*ux + uy*uy;

    dUY = -dGravConst;
    velConstA = 4.0*dUY*( uy + dUY);
    velConstB = dUY*(dUY - 2.0*uy);

    tau = dTau1 + (rhoLoc[0] - dRho1)*dRelTauRhoRatio;
    degRate = 0.5/tau;

    drvRho_1 = gradRho_1[element]; drvRho_2 = gradRho_2[element];
    gradRhoU = ux*drvRho_1 + uy*drvRho_2;
    gradPsiU = ux*( 4.0*psiLoc[1] - 4.0*psiLoc[3] + psiLoc[5] - psiLoc[7] + psiLoc[8] - psiLoc[6] )*inv12 + 
      uy * ( 4.0*psiLoc[2] - 4.0*psiLoc[4] + psiLoc[5] - psiLoc[7] + psiLoc[6] - psiLoc[8] )*inv12;
    
    GX = 1.5*dKappa*( ( 4.0*gradRhoSq[e1] - 4.0*gradRhoSq[e3] + gradRhoSq[e5] - gradRhoSq[e7] + gradRhoSq[e8] - gradRhoSq[e6] )*inv12 -      // del_x(del_j Rho)^2
    ( 4.0*gradRho_11[e1] - 4.0*gradRho_11[e3] + gradRho_11[e5] - gradRho_11[e7] + gradRho_11[e8] - gradRho_11[e6] )*inv12 -   // del_x(del_xx rho)
    ( 4.0*gradRho_12[e2] - 4.0*gradRho_12[e4] + gradRho_12[e5] - gradRho_12[e7] + gradRho_12[e6] - gradRho_12[e8] )*inv12 );    // del_y(del_xy rho)
    GY = 1.5*dKappa*( ( 4.0*gradRhoSq[e2] - 4.0*gradRhoSq[e4] + gradRhoSq[e5] - gradRhoSq[e7] + gradRhoSq[e6] - gradRhoSq[e8] )*inv12 -
    ( 4.0*gradRho_22[e2] - 4.0*gradRho_22[e4] + gradRho_22[e5] - gradRho_22[e7] + gradRho_22[e6] - gradRho_22[e8] )*inv12 -
    ( 4.0*gradRho_12[e1] - 4.0*gradRho_12[e3] + gradRho_12[e5] - gradRho_12[e7] + gradRho_12[e8] - gradRho_12[e6] )*inv12 );  
    GU = -ux*GX -uy*GY;

    //-------------------------e0---------------------------------
    gam = wght1 * (1.0 - 1.5*u2);    
    gEqConst = 4.0/3.0*prssrLoc;
    gEq =  gEqConst + rhoLoc[0]*( gam -wght1 );
    force = 2.0/3.0*rhoLoc[0]*velConstB;

    frc = -0.5*gradRhoU*(gam - wght1) + (GU ) * gam;
    gLoc[0] = gLoc[0] + degRate *( gEq - gLoc[0] ) + frc +  0.5*force; 

    frc = (-0.5*gradRhoU +1.5*rhoLoc[0]*gradPsiU )*gam;
    fLoc[0] = fLoc[0] + degRate*( rhoLoc[0]*gam - fLoc[0] ) + frc;

    //-------------------------e1---------------------------------
    gam = wght2 + ( ux +1.5*ux*ux -0.5*u2)/3.0; 
    gEqConst = prssrLoc/3.0;
    gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*velConstB;

    frc = 0.5* (dirDerivRhoLoc[1] - gradRhoU) * (gam - wght2) + ( GX + GU )*gam;
    gLoc[1] = gLoc[1] + degRate*( gEq - gLoc[1] ) + frc +  0.5*force;  

    frc = 0.5*(dirDerivRhoLoc[1] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[1] - gradPsiU )*gam;
    fLoc[1] = fLoc[1] + degRate*( rhoLoc[0]*gam - fLoc[1] ) + frc;


    //-------------------------e3---------------------------------
    gam = wght2 + ( -ux +1.5*ux*ux -0.5*u2)/3.0; 
    gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*velConstB;

    frc = 0.5* (dirDerivRhoLoc[3] - gradRhoU) * (gam - wght2) + ( -GX + GU )*gam;
    gLoc[3] = gLoc[3] + degRate*( gEq - gLoc[3] ) + frc +  0.5*force;    

    frc = 0.5*(dirDerivRhoLoc[3] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[3] - gradPsiU )*gam;
    fLoc[3] = fLoc[3] + degRate*( rhoLoc[0]*gam - fLoc[3] ) + frc;

    //-------------------------e4---------------------------------
    gam = wght2 + ( -uy +1.5*uy*uy -0.5*u2)/3.0; 
    gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*(-2.0*dUY + velConstA);

    frc = 0.5* (dirDerivRhoLoc[4] - gradRhoU) * (gam - wght2) + ( -GY + GU )*gam ;
    gLoc[4] = gLoc[4] + degRate*( gEq - gLoc[4] ) + frc +  0.5*force;    

    frc = 0.5*(dirDerivRhoLoc[4] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[4] - gradPsiU )*gam;
    fLoc[4] = fLoc[4] + degRate*( rhoLoc[0]*gam - fLoc[4] ) + frc;

    //-------------------------e7---------------------------------
    gEqConst = prssr[element]/12.0;
    gam = wght3 + ( -ux-uy +u2 +3.0*ux*uy)/12.0; 
    gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );
      force = 1.0/24.0*rhoLoc[0]*(-2.0*dUY + velConstA);

    frc = 0.5* (dirDerivRhoLoc[7] - gradRhoU) * (gam - wght3) + ( -GX -GY + GU )*gam;
    gLoc[7] = gLoc[7] + degRate*( gEq - gLoc[7] ) + frc +  0.5*force;

    frc = 0.5*(dirDerivRhoLoc[7] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[7] - gradPsiU )*gam;
    fLoc[7] = fLoc[7] + degRate*( rhoLoc[0]*gam - fLoc[7] ) + frc;    

    //-------------------------e8---------------------------------
    gam = wght3 + ( ux-uy +u2 -3.0*ux*uy)/12.0; 
    gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );
    force = 1.0/24.0*rhoLoc[0]*(-2.0*dUY + velConstA);

    frc = 0.5* (dirDerivRhoLoc[8] - gradRhoU) * (gam - wght3) + ( GX - GY + GU )*gam;
    gLoc[8] = gLoc[8] + degRate*( gEq - gLoc[8] ) + frc +  0.5*force;  

    frc = 0.5*(dirDerivRhoLoc[8] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[8] - gradPsiU)*gam;
    fLoc[8] = fLoc[8] + degRate*( rhoLoc[0]*gam - fLoc[8] ) + frc;  

    // --------------------------streaming--------------------------
    gBar_0[element] = gLoc[0]; 
    gBar_1[e1] = gLoc[1]; 
    gBar_3[e3] = gLoc[3]; 
    gBar_4[e4] = gLoc[4]; 
    gBar_7[e7] = gLoc[7]; 
    gBar_8[e8] = gLoc[8];

    fBar_0[element] = fLoc[0]; 
    fBar_1[e1] = fLoc[1]; 
    fBar_3[e3] = fLoc[3]; 
    fBar_4[e4] = fLoc[4]; 
    fBar_7[e7] = fLoc[7]; 
    fBar_8[e8] = fLoc[8];
  }

    // ----------------------------bottom boundary-----------------------------
  #pragma omp for
    for( int b=0; b<iLenX; b++){


    gLoc[0] = g_0[b]; gLoc[1] = g_1[b]; gLoc[2] = g_2[b]; gLoc[3] = g_3[b]; gLoc[4] = g_4[b]; 
    gLoc[5] = g_5[b]; gLoc[6] = g_6[b]; gLoc[7] = g_7[b]; gLoc[8] = g_8[b];
    fLoc[0] = f_0[b]; fLoc[1] = f_1[b]; fLoc[2] = f_2[b]; fLoc[3] = f_3[b]; fLoc[4] = f_4[b]; 
    fLoc[5] = f_5[b]; fLoc[6] = f_6[b]; fLoc[7] = f_7[b]; fLoc[8] = f_8[b];

    // local field of Next Nearest Neighbors (NNN)
    e1 = nnn_1[b];  e2 = nnn_2[b]; e3 = nnn_3[b]; e4 = nnn_4[b]; e5 = nnn_5[b]; e6 = nnn_6[b]; e7 = nnn_7[b]; e8 = nnn_8[b];
    rhoLoc[9] = rho[e1]; rhoLoc[10] = rho[e2]; rhoLoc[11] = rho[e3]; rhoLoc[12] = rho[e4];
    rhoLoc[13] = rho[e5]; rhoLoc[14] = rho[e6]; rhoLoc[15] = rho[e7]; rhoLoc[16] = rho[e8];

    psiLoc[9] = psi[e1]; psiLoc[10] = psi[e2]; psiLoc[11] = psi[e3]; psiLoc[12] = psi[e4];
    psiLoc[13] = psi[e5]; psiLoc[14] = psi[e6]; psiLoc[15] = psi[e7]; psiLoc[16] = psi[e8];

    // local field of Nearest Neighbors (NN)
    e1 = nn_1[b];  e2 = nn_2[b]; e3 = nn_3[b]; e4 = nn_4[b]; e5 = nn_5[b]; e6 = nn_6[b]; e7 = nn_7[b]; e8 = nn_8[b];

    rhoLoc[0] = rho[b]; rhoLoc[1] = rho[e1]; rhoLoc[2] = rho[e2]; rhoLoc[3] = rho[e3]; rhoLoc[4] = rho[e4];
    rhoLoc[5] = rho[e5]; rhoLoc[6] = rho[e6]; rhoLoc[7] = rho[e7]; rhoLoc[8] = rho[e8];

    psiLoc[0] = psi[b]; psiLoc[1] = psi[e1]; psiLoc[2] = psi[e2]; psiLoc[3] = psi[e3]; psiLoc[4] = psi[e4];
    psiLoc[5] = psi[e5]; psiLoc[6] = psi[e6]; psiLoc[7] = psi[e7]; psiLoc[8] = psi[e8];

    // -------------------------calculating derivatives------------------------------

    dirDerivRhoLoc[1] = 0.5*( -rhoLoc[9] +4.0*rhoLoc[1] -3.0*rhoLoc[0] );
    if( (rhoLoc[9] - rhoLoc[0])*dirDerivRhoLoc[1] < 0.0 ) dirDerivRhoLoc[1] = 0.5*( rhoLoc[1] - rhoLoc[3] );

    dirDerivRhoLoc[2] = 0.5*( -rhoLoc[10] +4.0*rhoLoc[2] -3.0*rhoLoc[0] );
    if( (rhoLoc[10] - rhoLoc[0])*dirDerivRhoLoc[2] < 0.0 ) dirDerivRhoLoc[2] = 0.5*( rhoLoc[2] - rhoLoc[4] );

    dirDerivRhoLoc[3] = 0.5*( -rhoLoc[11] +4.0*rhoLoc[3] -3.0*rhoLoc[0] );
    if( (rhoLoc[11] - rhoLoc[0])*dirDerivRhoLoc[3] < 0.0 ) dirDerivRhoLoc[3] = 0.5*( rhoLoc[3] - rhoLoc[1] );

    dirDerivRhoLoc[5] = 0.5*( -rhoLoc[13] +4.0*rhoLoc[5] -3.0*rhoLoc[0] );
    if( (rhoLoc[13] - rhoLoc[0])*dirDerivRhoLoc[5] < 0.0 ) dirDerivRhoLoc[5] = 0.5*( rhoLoc[5] - rhoLoc[7] );

    dirDerivRhoLoc[6] = 0.5*( -rhoLoc[14] +4.0*rhoLoc[6] -3.0*rhoLoc[0] );
    if( (rhoLoc[14] - rhoLoc[0])*dirDerivRhoLoc[6] < 0.0 ) dirDerivRhoLoc[6] = 0.5*( rhoLoc[6] - rhoLoc[8] );

    dirDerivPsiLoc[1] = 0.5*( -psiLoc[9] +4.0*psiLoc[1] -3.0*psiLoc[0] );
    if( (psiLoc[9] - psiLoc[0])*dirDerivPsiLoc[1] < 0.0 ) dirDerivPsiLoc[1] = 0.5*( psiLoc[1] - psiLoc[3] );

    dirDerivPsiLoc[2] = 0.5*( -psiLoc[10] +4.0*psiLoc[2] -3.0*psiLoc[0] );
    if( (psiLoc[10] - psiLoc[0])*dirDerivPsiLoc[2] < 0.0 ) dirDerivPsiLoc[2] = 0.5*( psiLoc[2] - psiLoc[4] );

    dirDerivPsiLoc[3] = 0.5*( -psiLoc[11] +4.0*psiLoc[3] -3.0*psiLoc[0] );
    if( (psiLoc[11] - psiLoc[0])*dirDerivPsiLoc[3] < 0.0 ) dirDerivPsiLoc[3] = 0.5*( psiLoc[3] - psiLoc[1] );

    dirDerivPsiLoc[5] = 0.5*( -psiLoc[13] +4.0*psiLoc[5] -3.0*psiLoc[0] );
    if( (psiLoc[13] - psiLoc[0])*dirDerivPsiLoc[5] < 0.0 ) dirDerivPsiLoc[5] = 0.5*( psiLoc[5] - psiLoc[7] );

    dirDerivPsiLoc[6] = 0.5*( -psiLoc[14] +4.0*psiLoc[6] -3.0*psiLoc[0] );
    if( (psiLoc[14] - psiLoc[0])*dirDerivPsiLoc[6] < 0.0 ) dirDerivPsiLoc[6] = 0.5*( psiLoc[6] - psiLoc[8] );

    prssrLoc = prssr[b];
    ux = u_1[b]; 
    uy = u_2[b]; 
    u2 = ux*ux + uy*uy;

    dUY = -dGravConst;
    velConstA = 4.0*dUY*( uy + dUY);
    velConstB = dUY*(dUY - 2.0*uy);

    tau = dTau1 + (rhoLoc[0] - dRho1)*dRelTauRhoRatio;
    degRate = 0.5/tau;
    drvRho_1 = gradRho_1[b]; drvRho_2 = gradRho_2[b];
    gradRhoU = ux*drvRho_1 + uy*drvRho_2;

    GX = 1.5*dKappa*( ( 4.0*gradRhoSq[e1] - 4.0*gradRhoSq[e3] + gradRhoSq[e5] - gradRhoSq[e7] + gradRhoSq[e8] - gradRhoSq[e6] )*inv12 -      // del_x(del_j Rho)^2
    ( 4.0*gradRho_11[e1] - 4.0*gradRho_11[e3] + gradRho_11[e5] - gradRho_11[e7] + gradRho_11[e8] - gradRho_11[e6] )*inv12 -   // del_x(del_xx rho)
    ( 4.0*gradRho_12[e2] - 4.0*gradRho_12[e4] + gradRho_12[e5] - gradRho_12[e7] + gradRho_12[e6] - gradRho_12[e8] )*inv12 );    // del_y(del_xy rho)
    GY = 1.5*dKappa*( ( 4.0*gradRhoSq[e2] - 4.0*gradRhoSq[e4] + gradRhoSq[e5] - gradRhoSq[e7] + gradRhoSq[e6] - gradRhoSq[e8] )*inv12 -
    ( 4.0*gradRho_22[e2] - 4.0*gradRho_22[e4] + gradRho_22[e5] - gradRho_22[e7] + gradRho_22[e6] - gradRho_22[e8] )*inv12 -
    ( 4.0*gradRho_12[e1] - 4.0*gradRho_12[e3] + gradRho_12[e5] - gradRho_12[e7] + gradRho_12[e8] - gradRho_12[e6] )*inv12 );  
    GU = -ux*GX -uy*GY;

    gradPsiU = ux*( 4.0*psiLoc[1] - 4.0*psiLoc[3] + psiLoc[5] - psiLoc[7] + psiLoc[8] - psiLoc[6] )*inv12 + 
    uy * ( 4.0*psiLoc[2] - 4.0*psiLoc[4] + psiLoc[5] - psiLoc[7] + psiLoc[6] - psiLoc[8] )*inv12;

    //-------------------------e0---------------------------------
    gam = wght1 * (1.0 - 1.5*u2);    
    gEqConst = 4.0/3.0*prssrLoc;
    gEq =  gEqConst + rhoLoc[0]*( gam -wght1 );
    force = 2.0/3.0*rhoLoc[0]*velConstB;

    frc = -0.5*gradRhoU*(gam - wght1) + GU * gam;
    gLoc[0] = gLoc[0] + degRate *( gEq - gLoc[0] ) + frc +  0.5*force; 

    frc = (-0.5*gradRhoU +1.5*rhoLoc[0]*gradPsiU )*gam;
    fLoc[0] = fLoc[0] + degRate*( rhoLoc[0]*gam - fLoc[0] ) + frc;

    //-------------------------e1---------------------------------
    gam = wght2 + ( ux +1.5*ux*ux -0.5*u2)/3.0; 
    gEqConst = prssrLoc/3.0;
    gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
    force = 1.0/6.0*rhoLoc[0]*velConstB;

    frc = 0.5* (dirDerivRhoLoc[1] - gradRhoU) * (gam - wght2) + ( GX + GU )*gam;
    gLoc[1] = gLoc[1] + degRate*( gEq - gLoc[1] ) + frc +  0.5*force;  

    frc = 0.5*(dirDerivRhoLoc[1] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[1] - gradPsiU )*gam;
    fLoc[1] = fLoc[1] + degRate*( rhoLoc[0]*gam - fLoc[1] ) + frc;

    //-------------------------e2---------------------------------
    gam = wght2 + ( uy +1.5*uy*uy -0.5*u2)/3.0; 
    gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
    force = 1.0/6.0*rhoLoc[0]*(2.0*dUY + velConstA);

    frc = 0.5* (dirDerivRhoLoc[2] - gradRhoU ) * (gam - wght2) + (GY + GU )*gam ;
    gLoc[2] = gLoc[2] + degRate*( gEq - gLoc[2] ) + frc +  0.5*force;  

    frc = 0.5*(dirDerivRhoLoc[2] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[2] - gradPsiU )*gam;
    fLoc[2] = fLoc[2] + degRate*( rhoLoc[0]*gam - fLoc[2] ) + frc;        

    //-------------------------e3---------------------------------
    gam = wght2 + ( -ux +1.5*ux*ux -0.5*u2)/3.0; 
    gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
    force = 1.0/6.0*rhoLoc[0]*velConstB;

    frc = 0.5* (dirDerivRhoLoc[3] - gradRhoU) * (gam - wght2) + ( -GX + GU )*gam;
    gLoc[3] = gLoc[3] + degRate*( gEq - gLoc[3] ) + frc +  0.5*force;    

    frc = 0.5*(dirDerivRhoLoc[3] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[3] - gradPsiU )*gam;
    fLoc[3] = fLoc[3] + degRate*( rhoLoc[0]*gam - fLoc[3] ) + frc;

    //-------------------------e5---------------------------------
    gam = wght3 + ( ux+uy +u2 +3.0*ux*uy)/12.0; 
    gEqConst = prssr[b]/12.0;
    gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );
    force = 1.0/24.0*rhoLoc[0]*(2.0*dUY + velConstA);

    frc = 0.5* (dirDerivRhoLoc[5] - gradRhoU) * (gam - wght3) + (GX + GY + GU )*gam;
    gLoc[5] = gLoc[5] + degRate*( gEq - gLoc[5] ) + frc +  0.5*force;

    frc = 0.5*(dirDerivRhoLoc[5] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[5] - gradPsiU )*gam;
    fLoc[5] = fLoc[5] + degRate*( rhoLoc[0]*gam - fLoc[5] ) + frc;    

    //-------------------------e6---------------------------------
    gam = wght3 + ( -ux+uy +u2 -3.0*ux*uy)/12.0; 
    gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );   
    force = 1.0/24.0*rhoLoc[0]*(2.0*dUY + velConstA);

    frc = 0.5* (dirDerivRhoLoc[6] - gradRhoU) * (gam - wght3) + ( -GX + GY + GU)*gam;
    gLoc[6] = gLoc[6] + degRate*( gEq - gLoc[6] ) + frc +  0.5*force;

    frc = 0.5*(dirDerivRhoLoc[6] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[6] - gradPsiU )*gam;
    fLoc[6] = fLoc[6] + degRate*( rhoLoc[0]*gam - fLoc[6] ) + frc;    

    // --------------------------streaming--------------------------

    gBar_0[b] = gLoc[0]; 
    gBar_1[e1] = gLoc[1]; 
    gBar_2[e2] = gLoc[2]; 
    gBar_3[e3] = gLoc[3]; 
    gBar_5[e5] = gLoc[5]; 
    gBar_6[e6] = gLoc[6]; 

    fBar_0[b] = fLoc[0]; 
    fBar_1[e1] = fLoc[1]; 
    fBar_2[e2] = fLoc[2]; 
    fBar_3[e3] = fLoc[3]; 
    fBar_5[e5] = fLoc[5]; 
    fBar_6[e6] = fLoc[6]; 
    
    } 
  
  #pragma omp for
    for(int b=0; b<iLenX; b++){
     
    // hard boundary on top/bottom side
      element = b+iLenX*(iLenY-1);    // top line element

  // free slip at top boundary 

      // gBar_4[element] = gBar_2[element] - 1.0/3.0*rho[element]*dGravConst;
      // gBar_7[element] = gBar_6[element] - 1.0/12.0*rho[element]*dGravConst;
      // gBar_8[element] = gBar_5[element] - 1.0/12.0*rho[element]*dGravConst;

      gBar_4[element] = gBar_2[element] ;
      gBar_7[element] = gBar_6[element] ;
      gBar_8[element] = gBar_5[element] ;

      fBar_4[element] = fBar_2[element];
      fBar_7[element] = fBar_6[element];
      fBar_8[element] = fBar_5[element];

      // fBar_4[element] = 1.0/3.0*(dRho2-(fBar_0[element] + fBar_1[element] + fBar_2[element] + fBar_3[element] + fBar_5[element] + fBar_6[element]));
      // fBar_7[element] = 1.0/3.0*(dRho2-(fBar_0[element] + fBar_1[element] + fBar_2[element] + fBar_3[element] + fBar_5[element] + fBar_6[element]));
      // fBar_8[element] = 1.0/3.0*(dRho2-(fBar_0[element] + fBar_1[element] + fBar_2[element] + fBar_3[element] + fBar_5[element] + fBar_6[element]));


      // gBar_4[element] = gBar_4[nn_4[element]] ;
      // gBar_7[element] = gBar_7[nn_7[element]] ;
      // gBar_8[element] = gBar_8[nn_8[element]] ;

      // fBar_4[element] = fBar_4[nn_4[element]];
      // fBar_7[element] = fBar_7[nn_7[element]];
      // fBar_8[element] = fBar_8[nn_8[element]];

  // free slip at bottom boundary
      // fBar_2[b] = 2.0/3.0*(dRho1-(fBar_0[b] + fBar_1[b] + fBar_3[b] + fBar_4[b] + fBar_7[b] + fBar_8[b]));
      // fBar_5[b] = 1.0/6.0*(dRho1-(fBar_0[b] + fBar_1[b] + fBar_3[b] + fBar_4[b] + fBar_7[b] + fBar_8[b]));
      // fBar_6[b] = 1.0/6.0*(dRho1-(fBar_0[b] + fBar_1[b] + fBar_3[b] + fBar_4[b] + fBar_7[b] + fBar_8[b]));

      // fBar_2[nn_2[b]] = 2.0/3.0*(dRho1-(fBar_0[nn_2[b]] + fBar_1[nn_2[b]] + fBar_3[nn_2[b]] + fBar_4[nn_2[b]] + fBar_7[nn_2[b]] + fBar_8[nn_2[b]]));
      // fBar_5[nn_5[b]] = 1.0/6.0*(dRho1-(fBar_0[nn_5[b]] + fBar_1[nn_5[b]] + fBar_3[nn_5[b]] + fBar_4[nn_5[b]] + fBar_7[nn_5[b]] + fBar_8[nn_5[b]]));
      // fBar_6[nn_6[b]] = 1.0/6.0*(dRho1-(fBar_0[nn_6[b]] + fBar_1[nn_6[b]] + fBar_3[nn_6[b]] + fBar_4[nn_6[b]] + fBar_7[nn_6[b]] + fBar_8[nn_6[b]]));

      // gBar_2[b] = 2.0/3.0*(3.0 -(gBar_0[b] + gBar_1[b] + gBar_3[b] + gBar_4[b] + gBar_7[b] + gBar_8[b]));
      // gBar_5[b] = 1.0/6.0*(3.0 -(gBar_0[b] + gBar_1[b] + gBar_3[b] + gBar_4[b] + gBar_7[b] + gBar_8[b]));
      // gBar_6[b] = 1.0/6.0*(3.0 -(gBar_0[b] + gBar_1[b] + gBar_3[b] + gBar_4[b] + gBar_7[b] + gBar_8[b]));

      fBar_2[b] = fBar_4[b];
      fBar_5[b] = fBar_8[b];
      fBar_6[b] = fBar_7[b];

      gBar_2[b] = gBar_4[b];
      gBar_5[b] = gBar_8[b];
      gBar_6[b] = gBar_7[b];

      // gBar_2[b] = gBar_4[b] + 1.0/3.0*rho[b]*dGravConst;
      // gBar_5[b] = gBar_8[b] + 1.0/12.0*rho[b]*dGravConst;
      // gBar_6[b] = gBar_7[b] + 1.0/12.0*rho[b]*dGravConst;

      // fBar_2[b] = fBar_2[nn_2[b]];
      // fBar_5[b] = fBar_5[nn_5[b]];
      // fBar_6[b] = fBar_6[nn_6[b]];

      // gBar_2[b] = gBar_2[nn_2[b]];
      // gBar_5[b] = gBar_5[nn_5[b]];
      // gBar_6[b] = gBar_6[nn_6[b]];
    }  
  

  // ----------------------------------------------------macroscopic-------------------------------------------------
  #pragma omp for
    // for( int b=iLenX; b<iLenAr; b++){
    for( int b=0; b<iLenAr; b++){
      gLoc[0] = gBar_0[b]; gLoc[1] = gBar_1[b]; gLoc[2] = gBar_2[b]; gLoc[3] = gBar_3[b]; gLoc[4] = gBar_4[b]; 
      gLoc[5] = gBar_5[b]; gLoc[6] = gBar_6[b]; gLoc[7] = gBar_7[b]; gLoc[8] = gBar_8[b];
      fLoc[0] = fBar_0[b]; fLoc[1] = fBar_1[b]; fLoc[2] = fBar_2[b]; fLoc[3] = fBar_3[b]; fLoc[4] = fBar_4[b]; 
      fLoc[5] = fBar_5[b]; fLoc[6] = fBar_6[b]; fLoc[7] = fBar_7[b]; fLoc[8] = fBar_8[b];
     
      drvRho_1 = gradRho_1[b]; drvRho_2 = gradRho_2[b], drvRho_11 = gradRho_11[b]; drvRho_22 = gradRho_22[b], drvRho_12 = gradRho_12[b];

      rhoLoc[0] = fLoc[0] + fLoc[1] + fLoc[2] + fLoc[3] + fLoc[4] + fLoc[5] + fLoc[6] + fLoc[7] + fLoc[8];
      
      ux = ( gLoc[1] - gLoc[3] + gLoc[5] - gLoc[7] + gLoc[8] - gLoc[6] + 0.5*dKappa*( drvRho_2*drvRho_12 - drvRho_1*drvRho_22) )/rhoLoc[0];
      // uy = ( gLoc[2] - gLoc[4] + gLoc[5] - gLoc[7] + gLoc[6] - gLoc[8] + 0.5*dKappa*( drvRho_1*drvRho_12 - drvRho_2*drvRho_11) )/rhoLoc[0];
      uy = ( gLoc[2] - gLoc[4] + gLoc[5] - gLoc[7] + gLoc[6] - gLoc[8] + 0.5*dKappa*( drvRho_1*drvRho_12 - drvRho_2*drvRho_11) )/rhoLoc[0] - 0.5*dGravConst;
      prssrLoc = ( gLoc[0] + gLoc[1] + gLoc[2] + gLoc[3] + gLoc[4] + gLoc[5] + gLoc[6] + gLoc[7] + gLoc[8] + 0.5*( ux*drvRho_1 + uy*drvRho_2  ) )/3.0;
      psiLoc[0] = 4.0*dBeta*(rhoLoc[0] - dRho1)*(rhoLoc[0] - dRho2)*(rhoLoc[0] -0.5*(dRho1 + dRho2)) - dKappa*(drvRho_11 + drvRho_22);

      rho[b] = rhoLoc[0];
      u_1[b] = ux;
      u_2[b] = uy;
      prssr[b] = prssrLoc;
      psi[b] = psiLoc[0];
    }

  // ----------------------------------------------------postStr-------------------------------------------------
  #pragma omp for
    for( int b=0; b<iLenAr; b++){

      gLoc[0] = gBar_0[b]; gLoc[1] = gBar_1[b]; gLoc[2] = gBar_2[b]; gLoc[3] = gBar_3[b]; gLoc[4] = gBar_4[b]; 
      gLoc[5] = gBar_5[b]; gLoc[6] = gBar_6[b]; gLoc[7] = gBar_7[b]; gLoc[8] = gBar_8[b];
      fLoc[0] = fBar_0[b]; fLoc[1] = fBar_1[b]; fLoc[2] = fBar_2[b]; fLoc[3] = fBar_3[b]; fLoc[4] = fBar_4[b]; 
      fLoc[5] = fBar_5[b]; fLoc[6] = fBar_6[b]; fLoc[7] = fBar_7[b]; fLoc[8] = fBar_8[b];

      // local field of Nearest Neighbors (NN)
      e1 = nn_1[b];  e2 = nn_2[b]; e3 = nn_3[b]; e4 = nn_4[b]; e5 = nn_5[b]; e6 = nn_6[b]; e7 = nn_7[b]; e8 = nn_8[b];

      rhoLoc[0] = rho[b]; rhoLoc[1] = rho[e1]; rhoLoc[2] = rho[e2]; rhoLoc[3] = rho[e3]; rhoLoc[4] = rho[e4];
      rhoLoc[5] = rho[e5]; rhoLoc[6] = rho[e6]; rhoLoc[7] = rho[e7]; rhoLoc[8] = rho[e8];

      psiLoc[0] = psi[b]; psiLoc[1] = psi[e1]; psiLoc[2] = psi[e2]; psiLoc[3] = psi[e3]; psiLoc[4] = psi[e4];
      psiLoc[5] = psi[e5]; psiLoc[6] = psi[e6]; psiLoc[7] = psi[e7]; psiLoc[8] = psi[e8];


      // -------------------------calculating derivatives------------------------------
      
      dirDerivRhoLoc[1] = 0.5*( rhoLoc[1] - rhoLoc[3] );
      dirDerivRhoLoc[2] = 0.5*( rhoLoc[2] - rhoLoc[4] );
      dirDerivRhoLoc[3] = 0.5*( rhoLoc[3] - rhoLoc[1] );
      dirDerivRhoLoc[4] = 0.5*( rhoLoc[4] - rhoLoc[2] );
      dirDerivRhoLoc[5] = 0.5*( rhoLoc[5] - rhoLoc[7] );
      dirDerivRhoLoc[6] = 0.5*( rhoLoc[6] - rhoLoc[8] );
      dirDerivRhoLoc[7] = 0.5*( rhoLoc[7] - rhoLoc[5] );
      dirDerivRhoLoc[8] = 0.5*( rhoLoc[8] - rhoLoc[6] );
      
      dirDerivPsiLoc[1] = 0.5*( psiLoc[1] - psiLoc[3] );
      dirDerivPsiLoc[2] = 0.5*( psiLoc[2] - psiLoc[4] );
      dirDerivPsiLoc[3] = 0.5*( psiLoc[3] - psiLoc[1] );
      dirDerivPsiLoc[4] = 0.5*( psiLoc[4] - psiLoc[2] );
      dirDerivPsiLoc[5] = 0.5*( psiLoc[5] - psiLoc[7] );
      dirDerivPsiLoc[6] = 0.5*( psiLoc[6] - psiLoc[8] );
      dirDerivPsiLoc[7] = 0.5*( psiLoc[7] - psiLoc[5] );
      dirDerivPsiLoc[8] = 0.5*( psiLoc[8] - psiLoc[6] );

      prssrLoc = prssr[b];
      ux = u_1[b]; 
      uy = u_2[b]; 
      u2 = ux*ux + uy*uy;

     dUY = -dGravConst;
      velConstA = 4.0*dUY*( uy + dUY);
      velConstB = dUY*(dUY - 2.0*uy);

      tau = dTau1 + (rhoLoc[0] - dRho1)*dRelTauRhoRatio;
      degRate = 1.0/(2.0*tau +1);
      degRate2 =  tau/(tau+0.5);
      drvRho_1 = gradRho_1[b]; drvRho_2 = gradRho_2[b];
      gradRhoU = ux*drvRho_1 + uy*drvRho_2;

      // arraysentries abspeichern?
      GX = 1.5*dKappa*( ( 4.0*gradRhoSq[e1] - 4.0*gradRhoSq[e3] + gradRhoSq[e5] - gradRhoSq[e7] + gradRhoSq[e8] - gradRhoSq[e6] )*inv12 -      // del_x(del_j Rho)^2
        ( 4.0*gradRho_11[e1] - 4.0*gradRho_11[e3] + gradRho_11[e5] - gradRho_11[e7] + gradRho_11[e8] - gradRho_11[e6] )*inv12 -   // del_x(del_xx rho)
        ( 4.0*gradRho_12[e2] - 4.0*gradRho_12[e4] + gradRho_12[e5] - gradRho_12[e7] + gradRho_12[e6] - gradRho_12[e8] )*inv12 );    // del_y(del_xy rho)
      GY = 1.5*dKappa*( ( 4.0*gradRhoSq[e2] - 4.0*gradRhoSq[e4] + gradRhoSq[e5] - gradRhoSq[e7] + gradRhoSq[e6] - gradRhoSq[e8] )*inv12 -
        ( 4.0*gradRho_22[e2] - 4.0*gradRho_22[e4] + gradRho_22[e5] - gradRho_22[e7] + gradRho_22[e6] - gradRho_22[e8] )*inv12 -
        ( 4.0*gradRho_12[e1] - 4.0*gradRho_12[e3] + gradRho_12[e5] - gradRho_12[e7] + gradRho_12[e8] - gradRho_12[e6] )*inv12 );  
      GU = -ux*GX -uy*GY;

      gradPsiU = ux*( 4.0*psiLoc[1] - 4.0*psiLoc[3] + psiLoc[5] - psiLoc[7] + psiLoc[8] - psiLoc[6] )*inv12 + 
        uy * ( 4.0*psiLoc[2] - 4.0*psiLoc[4] + psiLoc[5] - psiLoc[7] + psiLoc[6] - psiLoc[8] )*inv12;

      //-------------------------e0---------------------------------
      gam = wght1 * (1.0 - 1.5*u2);    
      gEqConst = 4.0/3.0*prssrLoc;
      gEq =  gEqConst + rhoLoc[0]*( gam -wght1 );
      force = 2.0/3.0*rhoLoc[0]*velConstB;

      frc = degRate2*(-0.5*gradRhoU*(gam - wght1) + GU  * gam);
      gLoc[0] = gLoc[0] + degRate *( gEq - gLoc[0] ) + frc +  degRate2*0.5*force; 

      frc = degRate2*(-0.5*gradRhoU +1.5*rhoLoc[0]*gradPsiU  )*gam;
      fLoc[0] = fLoc[0] + degRate*( rhoLoc[0]*gam - fLoc[0] ) + frc;

      //-------------------------e1---------------------------------
      gam = wght2 + ( ux +1.5*ux*ux -0.5*u2)/3.0; 
      gEqConst = prssrLoc/3.0;
      gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*velConstB;

      frc = degRate2*(0.5* (dirDerivRhoLoc[1] - gradRhoU) * (gam - wght2) + ( GX + GU )*gam);
      gLoc[1] = gLoc[1] + degRate*( gEq - gLoc[1] ) + frc +  degRate2*0.5*force;  

      frc = degRate2*(0.5*(dirDerivRhoLoc[1] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[1] - gradPsiU )*gam);
      fLoc[1] = fLoc[1] + degRate*( rhoLoc[0]*gam - fLoc[1] ) + frc;

      //-------------------------e2---------------------------------
      gam = wght2 + ( uy +1.5*uy*uy -0.5*u2)/3.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*(2.0*dUY + velConstA);

      frc = degRate2*(0.5* (dirDerivRhoLoc[2] - gradRhoU) * (gam - wght2) + (GY + GU )*gam);
      gLoc[2] = gLoc[2] + degRate*( gEq - gLoc[2] ) + frc +  degRate2*0.5*force;  

      frc = degRate2*(0.5*(dirDerivRhoLoc[2] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[2] - gradPsiU )*gam) ;
      fLoc[2] = fLoc[2] + degRate*( rhoLoc[0]*gam - fLoc[2] ) + frc;        

      //-------------------------e3---------------------------------
      gam = wght2 + ( -ux +1.5*ux*ux -0.5*u2)/3.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*velConstB;

      frc = degRate2*(0.5* (dirDerivRhoLoc[3] - gradRhoU) * (gam - wght2) + ( -GX + GU )*gam);
      gLoc[3] = gLoc[3] + degRate*( gEq - gLoc[3] ) + frc +  degRate2*0.5*force;    

      frc = degRate2*(0.5*(dirDerivRhoLoc[3] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[3] - gradPsiU )*gam);
      fLoc[3] = fLoc[3] + degRate*( rhoLoc[0]*gam - fLoc[3] ) + frc;

      //-------------------------e4---------------------------------
      gam = wght2 + ( -uy +1.5*uy*uy -0.5*u2)/3.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght2 );
      force = 1.0/6.0*rhoLoc[0]*(-2.0*dUY + velConstA);
      frc = degRate2*(0.5* (dirDerivRhoLoc[4] - gradRhoU) * (gam - wght2) + ( -GY + GU )*gam);
      gLoc[4] = gLoc[4] + degRate*( gEq - gLoc[4] ) + frc +  degRate2*0.5*force;    

      frc = degRate2*(0.5*(dirDerivRhoLoc[4] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[4] - gradPsiU )*gam);
      fLoc[4] = fLoc[4] + degRate*( rhoLoc[0]*gam - fLoc[4] ) + frc;

      //-------------------------e5---------------------------------
      gam = wght3 + ( ux+uy +u2 +3.0*ux*uy)/12.0; 
      gEqConst = prssr[b]/12.0;
      gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );
      force = 1.0/24.0*rhoLoc[0]*(2.0*dUY + velConstA);

      frc = degRate2*(0.5* (dirDerivRhoLoc[5] - gradRhoU) * (gam - wght3) + (GX + GY + GU )*gam);
      gLoc[5] = gLoc[5] + degRate*( gEq - gLoc[5] ) + frc +  degRate2*0.5*force;

      frc = degRate2*(0.5*(dirDerivRhoLoc[5] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[5] - gradPsiU )*gam);
      fLoc[5] = fLoc[5] + degRate*( rhoLoc[0]*gam - fLoc[5] ) + frc;    

      //-------------------------e6---------------------------------
      gam = wght3 + ( -ux+uy +u2 -3.0*ux*uy)/12.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );   
      force = 1.0/24.0*rhoLoc[0]*(2.0*dUY + velConstA);

      frc = degRate2*(0.5* (dirDerivRhoLoc[6] - gradRhoU) * (gam - wght3) + ( -GX + GY + GU)*gam);
      gLoc[6] = gLoc[6] + degRate*( gEq - gLoc[6] ) + frc +  degRate2*0.5*force;

      frc = degRate2*(0.5*(dirDerivRhoLoc[6] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[6] - gradPsiU )*gam);
      fLoc[6] = fLoc[6] + degRate*( rhoLoc[0]*gam - fLoc[6] ) + frc;    

      //-------------------------e7---------------------------------
      gam = wght3 + ( -ux-uy +u2 +3.0*ux*uy)/12.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );
      force = 1.0/24.0*rhoLoc[0]*(-2.0*dUY + velConstA);

      frc = degRate2*(0.5* (dirDerivRhoLoc[7] - gradRhoU) * (gam - wght3) + ( -GX -GY + GU )*gam);
      gLoc[7] = gLoc[7] + degRate*( gEq - gLoc[7] ) + frc +  degRate2*0.5*force;

      frc = degRate2*(0.5*(dirDerivRhoLoc[7] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[7] - gradPsiU )*gam);
      fLoc[7] = fLoc[7] + degRate*( rhoLoc[0]*gam - fLoc[7] ) + frc;    

      //-------------------------e8---------------------------------
      gam = wght3 + ( ux-uy +u2 -3.0*ux*uy)/12.0; 
      gEq =  gEqConst + rhoLoc[0]*( gam -wght3 );
      force = 1.0/24.0*rhoLoc[0]*(-2.0*dUY + velConstA);

      frc = degRate2*(0.5* (dirDerivRhoLoc[8] - gradRhoU) * (gam - wght3) + ( GX - GY + GU )*gam);
      gLoc[8] = gLoc[8] + degRate*( gEq - gLoc[8] ) + frc +  degRate2*0.5*force;  

      frc = degRate2*(0.5*(dirDerivRhoLoc[8] - gradRhoU)*gam -1.5*rhoLoc[0]*( dirDerivPsiLoc[8] - gradPsiU )*gam);
      fLoc[8] = fLoc[8] + degRate*( rhoLoc[0]*gam - fLoc[8] ) + frc;  

      g_0[b] = gLoc[0]; 
      g_1[b] = gLoc[1]; 
      g_2[b] = gLoc[2]; 
      g_3[b] = gLoc[3]; 
      g_4[b] = gLoc[4]; 
      g_5[b] = gLoc[5]; 
      g_6[b] = gLoc[6]; 
      g_7[b] = gLoc[7]; 
      g_8[b] = gLoc[8];

      f_0[b] = fLoc[0]; 
      f_1[b] = fLoc[1]; 
      f_2[b] = fLoc[2]; 
      f_3[b] = fLoc[3]; 
      f_4[b] = fLoc[4]; 
      f_5[b] = fLoc[5]; 
      f_6[b] = fLoc[6]; 
      f_7[b] = fLoc[7]; 
      f_8[b] = fLoc[8];
    }
  } // parallel loop
}
