// opt1DarOpemMp.cpp
// optimised version of drplsplsh with one dimensionsl arrays, without latticeNode.cpp one multithreading
#include "latticeInv.h"



int main(){
  printf("\n\n\n\n\t\t\thallo, ich bin invMain.cpp\n\n" );
  lattice lt;

  // for log output:
  char chBuffer [50];
  int iCharLen;


  if( lt.bAbort ){
    printf("aborting simulation.\n" );
    return 1;
  }

  lt.initialise();
  lt.deletePreviousData();
  lt.exportData();

  
  double meanTime = 0.0;
  double startTime;     
  double endTime;  

  int counter = 1;

  while (lt.iIterStep < lt.iIterStop ){
    while (lt.iIterStep < lt.iIterBreak && lt.iIterStep < lt.iIterStop){
      startTime = omp_get_wtime();
      
      lt.iterStep();
     
      endTime = omp_get_wtime();
      meanTime += endTime - startTime;
      lt.iIterStep += 1;
    }
    // lt.dump( lt.iObserveX, lt.iObserveY);
    if( lt.abort(lt.iIterStep)!= -1){ return 1; };        // testing if programm is still on tour
    lt.findInterface(counter);
    lt.findDropletAmplitude(counter);
    lt.findBubbleAmplitude(counter);
    lt.exportData();
   
    counter++;

    // printf("progress:\t%.1f%%\n",double(lt.iIterStep)/lt.iIterStop*100);
    iCharLen = sprintf( chBuffer, "progress:\t%.1f%%\n",double(lt.iIterStep)/lt.iIterStop*100 );
    lt.printLog( chBuffer, LOGOUT );
    
    lt.iIterBreak += lt.iIterInterval;
  }
  printf("mean time for iteration = %fms\n",meanTime/lt.iIterStep*1000); 

  lt.exportPropagation();

  if(lt.bGnuPlot){
    lt.gnuPlotPrint();
  }
	return 0;
}
